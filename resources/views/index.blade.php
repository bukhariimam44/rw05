@extends('layouts.umum')
@section('content')
<section class="main-news-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="single-main-news">
                            <a href="{{route('detail-berita',$utamas->judul)}}">
                                <img src="{{asset('assets/gambar/berita/'.$utamas->gambar)}}" alt="image">
                            </a>
                            <div class="news-content">
                                <div class="tag">{{$utamas->KategoriId->kategori}}</div>
                                <h3>
                                    <a href="{{route('detail-berita',str_replace(' ','_',$utamas->judul))}}">{{$utamas->judul}}</a>
                                </h3>
                                <span><a href="#">{{$utamas->UserId->name}}</a> / {{date('d M, Y', strtotime($utamas->created_at))}}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="single-main-news-inner">
                                <img src="{{asset('assets/gambar/tci.jpg')}}" alt="image">
                        </div>

                        <div class="single-main-news-box">
                            <a href="#">
                                <img src="{{asset('assets/gambar/lambang_kota_cilegon.png')}}" alt="image">
                            </a>
                            <div class="news-content">
                                <div class="tag">TAMAN CILEGON INDAH</div>
                                <h3>
                                    <a href="#">RW 05</a>
                                </h3>
                                <h3>
                                    <a href="#">Media Informasi Warga RW 05 Taman Cilegon Indah</a>
                                </h3>
                                <hr/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Main News Area -->

        <!-- Start Default News Area -->
        <section class="default-news-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="most-popular-news">
                            <div class="section-title"> 
                                <h2>Paling Populer</h2> 
                            </div>
    
                            <div class="row">
                                <div class="col-lg-6">
                                    @if($all['umum'] != null)
                                    <div class="single-most-popular-news">
                                        <div class="popular-news-image">
                                            <a href="{{route('detail-berita',str_replace(' ','_',$all['umum']['judul']))}}">
                                                <img src="{{asset('assets/gambar/berita/'.$all['umum']['gambar'])}}" alt="image">
                                            </a>
                                        </div>
                                        
                                        <div class="popular-news-content">
                                            <span>{{$all['umum']['KategoriId']['kategori']}}</span>
                                            <h3>
                                                <a href="{{route('detail-berita',str_replace(' ','_',$all['umum']['judul']))}}">{{$all['umum']['judul']}}</a>
                                            </h3>
                                            <p><a href="#">{{$all['umum']['UserId']['name']}}</a> / {{date('d M, Y', strtotime($all['umum']['created_at']))}}</p>
                                        </div>
                                    </div>
                                    @else
                                    <div class="single-most-popular-news">
                                        <div class="popular-news-image">
                                            <a href="#">
                                                <img src="{{asset('assets/gambar/berita/download.jpeg')}}" alt="image">
                                            </a>
                                        </div>
    
                                        <div class="popular-news-content">
                                            <span>POLITIK</span>
                                            <h3>
                                                <a href="#">BELUM ADA BERITA</a>
                                            </h3>
                                            <p><a href="#">#####</a> / ##-##-####</p>
                                        </div>
                                    </div>
                                    @endif
                                    @if($all['politik'] != null)
                                    <div class="single-most-popular-news">
                                        <div class="popular-news-image">
                                            <a href="{{route('detail-berita',str_replace(' ','_',$all['politik']['judul']))}}">
                                                <img src="{{asset('assets/gambar/berita/'.$all['politik']['gambar'])}}" alt="image">
                                            </a>
                                        </div>
    
                                        <div class="popular-news-content">
                                            <span>{{$all['politik']['KategoriId']['kategori']}}</span>
                                            <h3>
                                                <a href="{{route('detail-berita',str_replace(' ','_',$all['politik']['judul']))}}">{{$all['politik']['judul']}}</a>
                                            </h3>
                                            <p><a href="#">{{$all['politik']['UserId']['name']}}</a> / {{date('d M, Y', strtotime($all['politik']['created_at']))}}</p>
                                        </div>
                                    </div>
                                    @else
                                    <div class="single-most-popular-news">
                                        <div class="popular-news-image">
                                            <a href="#">
                                                <img src="{{asset('assets/gambar/berita/download.jpeg')}}" alt="image">
                                            </a>
                                        </div>
    
                                        <div class="popular-news-content">
                                            <span>POLITIK</span>
                                            <h3>
                                                <a href="#">BELUM ADA BERITA</a>
                                            </h3>
                                            <p><a href="#">#####</a> / ##-##-####</p>
                                        </div>
                                    </div>
                                    @endif
                                </div>
    
                                <div class="col-lg-6">
                                @if($all['olahraga'] != null)
                                    <div class="most-popular-post">
                                        <div class="row align-items-center">
                                            <div class="col-lg-4 col-sm-4">
                                                <div class="post-image">
                                                    <a href="{{route('detail-berita',str_replace(' ','_',$all['olahraga']['judul']))}}">
                                                        <img src="{{asset('assets/gambar/berita/'.$all['olahraga']['gambar'])}}" alt="image">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-sm-8">
                                                <div class="post-content">
                                                    <span>{{$all['olahraga']['KategoriId']['kategori']}}</span>
                                                    <h3>
                                                        <a href="{{route('detail-berita',str_replace(' ','_',$all['olahraga']['judul']))}}">{{ substr($all['olahraga']['judul'], 0, 45) }}... </a>
                                                    </h3>
                                                    <p>{{date('d M, Y', strtotime($all['olahraga']['created_at']))}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="most-popular-post">
                                        <div class="row align-items-center">
                                            <div class="col-lg-4 col-sm-4">
                                                <div class="post-image">
                                                    <a href="#">
                                                        <img src="{{asset('assets/gambar/berita/download.jpeg')}}" alt="image">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-sm-8">
                                                <div class="post-content">
                                                    <span>Olahraga</span>
                                                    <h3>
                                                        <a href="#">BELUM ADA BERITA</a>
                                                    </h3>
                                                    <p>##-##-####</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if($all['teknologi'] != null)
                                    <div class="most-popular-post">
                                        <div class="row align-items-center">
                                            <div class="col-lg-4 col-sm-4">
                                                <div class="post-image">
                                                    <a href="{{route('detail-berita',str_replace(' ','_',$all['teknologi']['judul']))}}">
                                                        <img src="{{asset('assets/gambar/berita/'.$all['teknologi']['gambar'])}}" alt="image">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-sm-8">
                                                <div class="post-content">
                                                    <span>{{$all['teknologi']['KategoriId']['kategori']}}</span>
                                                    <h3>
                                                        <a href="{{route('detail-berita',str_replace(' ','_',$all['teknologi']['judul']))}}">{{ substr($all['teknologi']['judul'], 0, 45) }}... </a>
                                                    </h3>
                                                    <p>{{date('d M, Y', strtotime($all['teknologi']['created_at']))}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="most-popular-post">
                                        <div class="row align-items-center">
                                            <div class="col-lg-4 col-sm-4">
                                                <div class="post-image">
                                                    <a href="#">
                                                        <img src="{{asset('assets/gambar/berita/download.jpeg')}}" alt="image">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-sm-8">
                                                <div class="post-content">
                                                    <span>Teknologi</span>
                                                    <h3>
                                                        <a href="#">BELUM ADA BERITA</a>
                                                    </h3>
                                                    <p>##-##-####</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if($all['bisnis'] != null)
                                    <div class="most-popular-post">
                                        <div class="row align-items-center">
                                            <div class="col-lg-4 col-sm-4">
                                                <div class="post-image">
                                                    <a href="{{route('detail-berita',str_replace(' ','_',$all['bisnis']['judul']))}}">
                                                        <img src="{{asset('assets/gambar/berita/'.$all['bisnis']['gambar'])}}" alt="image">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-sm-8">
                                                <div class="post-content">
                                                    <span>{{$all['bisnis']['KategoriId']['kategori']}}</span>
                                                    <h3>
                                                        <a href="{{route('detail-berita',str_replace(' ','_',$all['bisnis']['judul']))}}">{{ substr($all['bisnis']['judul'], 0, 45) }}... </a>
                                                    </h3>
                                                    <p>{{date('d M, Y', strtotime($all['bisnis']['created_at']))}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                <div class="most-popular-post">
                                        <div class="row align-items-center">
                                            <div class="col-lg-4 col-sm-4">
                                                <div class="post-image">
                                                    <a href="#">
                                                        <img src="{{asset('assets/gambar/berita/download.jpeg')}}" alt="image">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-sm-8">
                                                <div class="post-content">
                                                    <span>Bisnis</span>
                                                    <h3>
                                                        <a href="#">BELUM ADA BERITA</a>
                                                    </h3>
                                                    <p>##-##-####</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if($all['budaya'] != null)
                                    <div class="most-popular-post">
                                        <div class="row align-items-center">
                                            <div class="col-lg-4 col-sm-4">
                                                <div class="post-image">
                                                    <a href="{{route('detail-berita',str_replace(' ','_',$all['budaya']['judul']))}}">
                                                        <img src="{{asset('assets/gambar/berita/'.$all['budaya']['gambar'])}}" alt="image">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-sm-8">
                                                <div class="post-content">
                                                    <span>{{$all['budaya']['KategoriId']['kategori']}}</span>
                                                    <h3>
                                                        <a href="{{route('detail-berita',str_replace(' ','_',$all['budaya']['judul']))}}">{{ substr($all['budaya']['judul'], 0, 45) }}... </a>
                                                    </h3>
                                                    <p>{{date('d M, Y', strtotime($all['budaya']['created_at']))}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                <div class="most-popular-post">
                                        <div class="row align-items-center">
                                            <div class="col-lg-4 col-sm-4">
                                                <div class="post-image">
                                                    <a href="#">
                                                        <img src="{{asset('assets/gambar/berita/download.jpeg')}}" alt="image">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="col-lg-8 col-sm-8">
                                                <div class="post-content">
                                                    <span>Budaya</span>
                                                    <h3>
                                                        <a href="#">BELUM ADA BERITA</a>
                                                    </h3>
                                                    <p>##-##-####</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                </div>
                            </div>
                        </div>

                        <div class="health-news">
                            <div class="section-title"> 
                                <h2>Hiburan</h2> 
                            </div>

                            <div class="health-news-slides owl-carousel owl-theme">
                            @foreach($all['hiburan'] as $hbr)
                                <div class="single-health-news">
                                    <div class="health-news-image">
                                        <a href="{{route('detail-berita',str_replace(' ','_',$hbr['judul']))}}">
                                            <img src="{{asset('assets/gambar/berita/'.$hbr['gambar'])}}" alt="image">
                                        </a>
                                    </div>
                                    
                                    <div class="health-news-content">
                                        <span>{{$hbr['KategoriId']['kategori']}}</span>
                                        <h3>
                                            <a href="{{route('detail-berita',str_replace(' ','_',$hbr['judul']))}}">{{ substr($hbr['judul'], 0, 45) }}...</a>
                                        </h3>
                                        <p><a href="#">{{$hbr['UserId']['name']}}</a> / {{date('d M, Y', strtotime($hbr['created_at']))}}</p>
                                    </div>
                                </div>
                            @endforeach
                            @if(count($all['hiburan']) < 1)
                               <div class="single-health-news">
                                    <div class="health-news-image">
                                        <a href="#">
                                            <img src="{{asset('assets/gambar/berita/download.jpeg')}}" alt="image">
                                        </a>
                                    </div>
                                    
                                    <div class="health-news-content">
                                        <span>CommingSoon</span>
                                        <h3>
                                            <a href="#">BELUM ADA BERITA</a>
                                        </h3>
                                        <p><a href="#">#######</a> / ### ####, ####</p>
                                    </div>
                                </div>
                            @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <aside class="widget-area">
                            <section class="widget widget_latest_news_thumb">
                                <h3 class="widget-title">Berita Pengajuan Warga</h3>

                                @foreach($berita_wargas as $berita_warga)
                                <article class="item">
                                    <a href="{{route('detail-berita',str_replace(' ','_',$berita_warga['judul']))}}" class="thumb">
                                    <img class="fullimage cover" src="{{asset('assets/gambar/berita/'.$berita_warga->gambar)}}" alt="">
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="{{route('detail-berita',str_replace(' ','_',$berita_warga->judul))}}">{{ substr($berita_warga->judul, 0, 40) }}..</a></h4>
                                        <span>{{date('d M Y', strtotime($berita_warga->created_at))}}</span>
                                    </div>
                                </article>
                                @endforeach
                                @if(count($berita_wargas) < 1)
                                <article class="item">
                                    <a href="#" class="thumb">
                                    <img class="fullimage cover" src="{{asset('assets/gambar/berita/download.jpeg')}}" alt="">
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="#">Belum ada berita</a></h4>
                                        <span>## ####, ####</span>
                                    </div>
                                </article>
                                @endif
                            </section>

                            @include('includes.berlangganan')
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        @endsection