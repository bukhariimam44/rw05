@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
            <div class="container">
                <div class="page-title-content">
                    <h2>Struktur Organisasi</h2>
                    <ul>
                        <li><a href="{{route('index')}}">Beranda</a></li>
                        <li>Struktur Organisasi</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Start About Area -->
        <section class="about-area ptb-50">
            <div class="container">
                <div class="about-content">
                    <p>{!! $datas->konten !!}</p>
                </div>
                @if(Auth::check())
                @if(Auth::user()->type == 'Admin')
                <center><a href="{{route('edit-struktur-organisasi')}}" class="btn btn-success"><i class='bx bxs-edit'></i> Edit Struktur Organisasi</a></center>
                @endif
                @endif
            </div>
        </section>
        <!-- End About Area -->
@endsection