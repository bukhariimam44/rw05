@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
            <div class="container">
                <div class="page-title-content">
                    <h2>Tentang</h2>
                    <ul>
                        <li><a href="{{route('index')}}">Beranda</a></li>
                        <li>Tentang</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Page Banner -->

        <!-- Start About Area -->
        <section class="about-area ptb-50">
            <div class="container">
                <div class="about-content">
                    <p>{!! $datas->konten !!}</p>
                </div>
                @if(Auth::check())
                @if(Auth::user()->type == 'Admin')
                <center><a href="{{route('edit-tentang')}}" class="btn btn-warning"><i class='bx bxs-edit'></i> Edit Tentang</a></center>
                @endif
                @endif
            </div>
        </section>
        <!-- End About Area -->
        <div class="page-title-area">
            <div class="container">
                <div class="page-title-content">
                    <h2>Pengurus</h2>
                </div>
            </div>
        </div>
        <section class="team-area pt-50">
            <div class="container">
            @if(Auth::check())
            @if(Auth::user()->type == 'Admin')
            <a href="{{route('tambah-pengurus')}}" class="btn btn-success"><i class='bx bx-plus-circle'></i> Tambah Pengurus</a> <hr>
            @endif
            @endif
                <div class="row">
                @foreach($penguruses as $key => $pengurus)
                    <div class="col-lg-4 col-md-6">
                        <div class="single-team-box">
                            <div class="image">
                                <img src="{{asset('assets/gambar/pengurus/'.$pengurus->foto)}}" alt="image">
                                @if(Auth::check())
                                @if(Auth::user()->type == 'Admin')
                                <ul class="social">
                                    <li>
                                        <a href="{{route('edit-pengurus',$pengurus->id)}}">
                                        <i class='bx bxs-edit'></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="event.preventDefault();
                                                     document.getElementById('delete-form{{$pengurus->id}}').submit();">
                                        <i class='bx bxs-trash'></i>
                                        </a>
                                    </li>
                                </ul>
                                @endif
                                @endif
                            </div>

                            <div class="content">
                                <h3>{{$pengurus->nama}}</h3>
                                <span>{{$pengurus->jabatanId->jabatan}}</span>
                            </div>
                        </div>
                    </div>
                    <form id="delete-form{{$pengurus->id}}" action="{{route('edit-pengurus',$pengurus->id)}}" method="POST" style="display: none;">
                        @csrf
                        <input type="hidden" name="nama" value="{{$pengurus->nama}}" required>
                        <input type="hidden" name="sebagai" value="{{$pengurus->sebagai}}" required>
                        <input type="hidden" value="hapus" name="action">
                    </form>
                @endforeach
                @if(count($penguruses) < 1)
                    <div class="col-lg-12 col-md-12">
                        <div class="mb-30">
                        <center><label for="" class="text-center">Data Pengurus Belum ada</label></center>
                        </div>
                    </div>
                @endif
                </div>
            </div>
        </section>
@endsection