<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>Laporan</title>
    <style>
        body{
            font-family: Courier;
        }
        h3{
            font-family: Verdana, sans-serif;
        }
        span{
            font-size:14px;
        }
    </style>
</head>
<body>
    <center><h3 class="huruf">PERUMAHAN TAMAN CILEGON INDAH <br>RW 05 KELURAHAN SUKMAJAYA <br>KEC. JOMBANG KOTA CILEGON BANTEN</h3></center>
    <hr>
    <center><h3>LAPORAN KEUANGAN</h3></center><br>
    <table border="1">
        <tr>
            <td style="border: 1px solid black; padding:5px;"><span>BULAN : {{strtoupper(date('M Y', strtotime($data['tahun_bulan']))) }}</span></td>
        </tr>
    </table>

    <table width="100%">
        <tr>
            <td width="40px"><span>I.</span></td><td><span>SALDO BULAN {{strtoupper(date('M Y', strtotime('-1 month', strtotime( $data['tahun_bulan'] ))))}}</span></td>
            <td width="180px">
                <table border="1"  width="100%">
                    <tr>
                        <td style="border: 1px solid black; padding:5px;" align="right"><span>Rp {{number_format($data['saldo_awal'],0,',','.')}} </span></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td width="40px"><span>II.</span></td><td><span>PEMASUKAN</span> </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td width="40px"></td>
            <td>
                <table width="100%" style="margin-left:38px;">
                    <?php $total = 0;?>
                    @foreach($data['masuk'] as $key => $dt)
                    <?php $total+=$dt['nominal'];?>
                    <tr>
                        <td width="40px"><span>{{$key+1}}.</span></td><td><span>{{strtoupper($dt['keterangan'])}}</span></td><td align="right"><span>Rp {{number_format($dt['nominal'],0,',','.')}}</span></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="3"><hr></td>
                    </tr>
                    <tr>
                        <td colspan="2"><span>JUMLAH PEMASUKAN :</span> </td><td align="right"><span>Rp {{number_format($total,0,',','.')}}</span></td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <td width="40px"><span>III.</span></td><td><span>PENGELUARAN </span></td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td width="40px"></td>
            <td>
                <table width="100%" style="margin-left:38px;">
                    <?php $totalkl = 0;?>
                    @foreach($data['keluar'] as $key => $dt)
                    <?php $totalkl+=$dt['nominal'];?>
                    <tr>
                        <td width="40px"><span>{{$key+1}}.</span></td><td><span>{{strtoupper($dt['keterangan'])}}</span></td><td align="right"><span>Rp {{number_format($dt['nominal'],0,',','.')}}</span></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="3"><hr></td>
                    </tr>
                    <tr>
                        <td colspan="2"><span>JUMLAH PENGELUARAN : </span></td><td align="right"><span>Rp {{number_format($totalkl,0,',','.')}}</span></td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <td width="40px"><span>IV.</span></td><td><span>SISA (PEMASUKAN - PENGELUARAN) </span></td><td align="right"><span>(Rp {{number_format($total-$totalkl,0,',','.')}})<s/pan></td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <td width="40px"><span>V.</span></td><td><span>TOTAL SALDO BULAN {{strtoupper(date('M Y', strtotime($data['tahun_bulan']))) }}</span></td>
            <td align="right"  width="180px">
                <table border="1"  width="100%">
                    <tr>
                        <td style="border: 1px solid black; padding:5px;" align="right"><span>Rp {{number_format($data['saldo_akhir'],0,',','.')}} </span></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br><br>
    <table width="100%">
        <tr>
            <td align="center">
            <span>Disetujui Oleh<span>
                <br><br><br><br>
                <u>{{$data['ketua']['jabatanId']['jabatan']}} RW 05</u><br>
                <label for="">{{$data['ketua']['nama']}}</label>

            </td>
            <td align="center">
                <span>Dilaporkan Oleh<span>
                <br><br><br><br>
                <u>{{$data['sekretaris']['jabatanId']['jabatan']}} RW 05</u><br>
                <label for="">{{$data['sekretaris']['nama']}}</label>

            </td>
        </tr>
    </table>
</body>
</html>