@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h2>Laporan Keuangan</h2>
            <ul>
            <li><a href="{{route('home')}}">Home</a></li>
                <li>Laporan Keuangan</li>
            </ul>
        </div>
    </div>
</div>
        <!-- End Page Banner -->

        <section class="news-area ptb-50">
            <div class="container">
                <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <th>No.</th>
                                <th>Bulan</th>
                                <th>Saldo Awal</th>
                                <th>Tot. Masuk</th>
                                <th>Tot. Keluar</th>
                                <th>Saldo Akhir</th>
                                <th width="150px">Action</th>
                            </thead>
                            <tbody>
                                <?php $saldo_awal = 0;
                                $saldo_akhir = 0;?>
                                @foreach($datas as $key => $data)
                                <?php if ($key == 0) {
                                    $saldo_awal = $saldo_awal;
                                    $saldo_akhir = $saldo_awal + $data['akumulasi'];
                                }else {
                                    $saldo_awal =$saldo_akhir;
                                    $saldo_akhir = $saldo_awal + $data['akumulasi'];
                                }?>
                                <form action="{{route('laporan-keuangan')}}" method="post" id="download{{$key}}">
                                @csrf
                                <input type="hidden" name="tahun_bulan" value="{{$data['tahun_bulan']}}">
                                <input type="hidden" name="saldo_awal" value="{{$saldo_awal}}">
                                <input type="hidden" name="saldo_akhir" value="{{$saldo_akhir}}">
                                </form>
                                    <tr>
                                        <td>{{$key+1}}.</td>
                                        <td>{{date('M Y', strtotime($data['tahun_bulan']))}}</td>
                                        <td>{{number_format($saldo_awal,0,',','.')}}</td>
                                        <td>{{number_format($data['total_masuk'],0,',','.')}}</td>
                                        <td>{{number_format($data['total_keluar'],0,',','.')}}</td>
                                        <td>{{number_format($saldo_akhir,0,',','.')}}</td>
                                        <td>
                                            <a href="" class="btn btn-success" onclick="event.preventDefault();
                                                     document.getElementById('download{{$key}}').submit();">Download</a> 
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                    
                </div>
            </div>
        </section>

@endsection