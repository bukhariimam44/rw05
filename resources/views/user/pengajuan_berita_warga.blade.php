@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h2>Berita Pengajuan Warga</h2>
            <ul>
                <li><a href="{{route('home')}}">Home</a></li>
                <li>Pengajuan Warga</li>
            </ul>
        </div>
    </div>
</div>
        <!-- End Page Banner -->

        <section class="news-area ptb-50">
            <div class="container">
                <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <th width="20px">No.</th>
                                <th width="70px">Gambar</th>
                                <th>Judul</th>
                                <th>Dari Warga</th>
                                <th width="100px">Action</th>
                            </thead>
                            <tbody>
                                @foreach($datas as $key => $data)
                                    <tr>
                                        <td>{{$key+1}}.</td>
                                        <td width="80px">
                                        <img src="{{asset('assets/gambar/berita/'.$data->gambar)}}" alt="image">
                                        </td>
                                        <td>{{$data->judul}}</td>
                                        <td>{{$data->userId->name}} (RT.{{$data->userId->rt}})</td>
                                        <td>
                                        <a href="{{route('setuju-berita-warga',str_replace(' ','_',$data->judul))}}" class="btn btn-success">Setuju Tampilkan</a> <br><br>
                                        <a href="{{route('detail-berita',str_replace(' ','_',$data->judul))}}" class="btn btn-warning">Detail Berita</a> 
                                        </td>
                                    </tr>
                                @endforeach
                                @if(count($datas) < 1)
                                <tr>
                                    <td align="center" colspan="5">Berita belum ada</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                    
                </div>
            </div>
        </section>

@endsection