@extends('layouts.umum')
@section('css')
<style>
.merah {
    border: none;
    background-color: #ff661f;
    color: #ffffff;
    width: 100%;
    height:50px;
    margin-top: 15px;
    border-radius: 5px;
    cursor: pointer;
    transition:0.5s;
}
select.form-control{
    margin-top:20px !important;
}
</style>
@endsection
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h2>Tambah User</h2>
            <ul>
            <li><a href="{{route('home')}}">Home</a></li>
                <li>Tambah User</li>
            </ul>
        </div>
    </div>
</div>
<section class="news-area ptb-50">
    <div class="container">
        <div class="login-form">
            <form method="post" action="{{route('tambah-user')}}">
                <input type="hidden" name="action" value="tambah">
            @csrf
                <div class="row">
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input type="text" class="form-control" name="name" placeholder="Nama Lengkap...">
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" placeholder="Email...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nomor HP</label>
                                    <input type="text" name="nohp" class="form-control" placeholder="Nomor HP...">
                                </div>
                            </div>
                        </div>
                                                
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>RT</label>
                                    <input type="text" name="rt" class="form-control" placeholder="RT...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>RW</label>
                                    <input type="text" value="05" class="form-control"  disabled>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label>Sebagai</label>
                            <div class="languages-list">
                                    <select name="sebagai">
                                        <option value="Admin">Admin</option>
                                        <option value="Warga">Warga</option>
                                    </select>
                                </div>
                        </div>
                    
                    <div class="col-lg-12">
                        <div class="form-group">
                            <button type="submit" class="merah">SIMPAN</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- End Page Banner -->
@endsection
@section('js')

@endsection