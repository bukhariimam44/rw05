@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h2>Mutasi Keuangan</h2>
            <ul>
            <li><a href="{{route('home')}}">Home</a></li>
                <li>Mutasi Keuangan</li>
            </ul>
        </div>
    </div>
</div>
        <!-- End Page Banner -->

        <section class="news-area ptb-50">
            <div class="container">
                <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <th width="20px">No.</th>
                                <th width="200px">No. Transaksi <br>Tgl. Transaksi</th>
                                <th>Keluar</th>
                                <th>Masuk</th>
                                <th>Saldo</th>
                                <th>Keterangan</th>
                            </thead>
                            <tbody>
                                <?php $saldo = 0;?>
                                @foreach($datas as $key => $data)
                                    <tr>
                                        <td>{{$key+1}}.</td>
                                        <td>{{$data->no_trx}} <br>{{date('d M Y', strtotime($data->tgl_trx))}}</td>
                                        @if($data->mutasi == 'Kredit')
                                        <?php $saldo = $saldo + $data->nominal;?>
                                        <td>0</td>
                                        <td>{{number_format($data->nominal,0,',','.')}}</td>
                                        <td>{{number_format($saldo,0,',','.')}}</td>
                                        @else
                                        <?php $saldo = $saldo - $data->nominal;?>
                                        <td>{{number_format($data->nominal,0,',','.')}}</td>
                                        <td>0</td>
                                        <td>{{number_format($saldo,0,',','.')}}</td>
                                        @endif
                                        <td>{{$data->keterangan}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                    
                </div>
            </div>
        </section>

@endsection