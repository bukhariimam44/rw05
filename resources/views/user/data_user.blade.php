@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h2>Data User</h2>
            <ul>
            <li><a href="{{route('home')}}">Home</a></li>
                <li>Data User</li>
            </ul>
        </div>
    </div>
</div>
        <!-- End Page Banner -->

        <section class="news-area ptb-50">
            <div class="container">
                <div class="row">
                <div class="col-md-12">
                    @if(Auth::check())
                    @if(Auth::user()->type == 'Admin')
                    <a href="{{route('tambah-user')}}" class="btn btn-success"><i class='bx bx-plus-circle'></i> Tambah User</a> <hr>
                    @endif
                    @endif
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <th>No.</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>RT / RW</th>
                                <th>Sebagai</th>
                                <th width="300px">Action</th>
                            </thead>
                            <tbody>
                                @foreach($users as $key => $user)
                                    <tr>
                                        <td>{{$key+1}}.</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->rt}} / 05</td>
                                        <td>{{$user->type}}</td>
                                        <td>
                                        @if($user->email_verified_at == NULL)
                                        <a href="{{url('/user/aktifkan/'.$user->id)}}" class="btn btn-success">Aktifkan</a> 
                                        @else
                                        <a href="{{url('/user/nonaktifkan/'.$user->id)}}" class="btn btn-warning">NonAktifkan</a> 
                                        @endif
                                        <a href="{{url('/user/blokir/'.$user->id)}}" class="btn btn-danger">Blokir & Hapus</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                    
                </div>
            </div>
        </section>

@endsection