@extends('layouts.umum')
@section('css')
<style>
.merah {
    border: none;
    background-color: #ff661f;
    color: #ffffff;
    width: 100%;
    height:50px;
    margin-top: 15px;
    border-radius: 5px;
    cursor: pointer;
    transition:0.5s;
}
select.form-control{
    margin-top:20px !important;
}
</style>
@endsection
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h2>Tambah Uang Keluar</h2>
            <ul>
            <li><a href="{{route('home')}}">Home</a></li>
                <li>Tambah Uang Keluar</li>
            </ul>
        </div>
    </div>
</div>
<section class="news-area ptb-50">
    <div class="container">
        <div class="login-form">
            <form method="post" action="{{route('tambah-uang-keluar')}}">
                <input type="hidden" name="action" value="tambah">
            @csrf
                <div class="row">
                    <div class="form-group">
                        <label>Tanggal</label>
                        <input type="date" class="form-control" name="tanggal" placeholder="Tanggal...">
                    </div>
                    <div class="form-group">
                        <label>Nominal Rp</label>
                        <input type="text" class="form-control" id="nominal" name="nominal" placeholder="Nominal...">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea name="keterangan" id="" cols="30" rows="10" class="form-control"></textarea>
                    </div> 
                    
                    <div class="col-lg-12">
                        <div class="form-group">
                            <button type="submit" class="merah">SIMPAN</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- End Page Banner -->
@endsection
@section('js')
<script>
    var nominal = document.getElementById("nominal");
    nominal.addEventListener("keyup", function(e) {
        nominal.value = convertRupiah(this.value, "");
    });
    rupiah.addEventListener('keydown', function(event) {
        return isNumberKey(event);
    });
    function convertRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, "").toString(),
        split  = number_string.split(","),
        sisa   = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    
        if (ribuan) {
            separator = sisa ? "." : "";
            rupiah += separator + ribuan.join(".");
        }
    
        rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
        return prefix == undefined ? rupiah : rupiah ? prefix + rupiah : "";
    }
</script>
@endsection