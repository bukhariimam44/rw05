@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h2>Home</h2>
            <ul>
                <li><a href="{{route('index')}}">Beranda</a></li>
                <li>Home</li>
            </ul>
        </div>
    </div>
</div>
<!-- End Page Banner -->
@endsection