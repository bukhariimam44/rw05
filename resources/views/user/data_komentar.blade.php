@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h2>Data Komentar</h2>
            <ul>
                <li><a href="{{route('home')}}">Home</a></li>
                <li>Data Komentar</li>
            </ul>
        </div>
    </div>
</div>
        <!-- End Page Banner -->

        <section class="news-area ptb-50">
            <div class="container">
                <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <th>No.</th>
                                <th>Nama</th>
                                <!-- <th>Email</th>
                                <th>No HP/WA</th> -->
                                <th>Komentar</th>
                                <th width="100px">Action</th>
                            </thead>
                            <tbody>
                                @foreach($datas as $key => $data)
                                    <tr>
                                        <td>{{$key+1}}.</td>
                                        <td>{{$data->nama}} <br>{{$data->email}} <br>{{$data->nohp}}</td>
                                        <td>{{$data->komentar}}</td>
                                        <td>
                                        <a href="{{route('tampilkan-komentar',$data->id)}}" class="btn btn-success">Tampilkan</a> <br><br>
                                        <a href="{{route('detail-berita',str_replace(' ','_',$data->beritaId->judul))}}" class="btn btn-warning">Detail Berita</a> 
                                        </td>
                                    </tr>
                                @endforeach
                                @if(count($datas) < 1)
                                <tr>
                                    <td align="center" colspan="4">Komentar belum ada</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                    
                </div>
            </div>
        </section>

@endsection