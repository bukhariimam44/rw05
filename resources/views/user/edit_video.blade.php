@extends('layouts.umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
<style>
.merah {
    border: none;
    background-color: #ff661f;
    color: #ffffff;
    width: 100%;
    height:50px;
    margin-top: 15px;
    border-radius: 5px;
    cursor: pointer;
    transition:0.5s;
}
</style>
@endsection
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h2>Edit Video</h2>
            <ul>
            <li><a href="{{route('home')}}">Home</a></li>
                <li>Edit Video</li>
            </ul>
        </div>
    </div>
</div>
<section class="news-area ptb-50">
    <div class="container">
        <div class="login-form">
            <form method="post" action="{{route('edit-video',$edit->id)}}" enctype="multipart/form-data">
            @csrf
            <!-- <input type="hidden" name="ids" value=""> -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="">Judul Video</label>
                            <input type="text" name="judul" value="{{$edit->judul}}" class="form-control" required>
                        </div>
                    </div>
                    
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="">Key Video Youtube</label>
                            <input type="text" name="video" value="{{$edit->key_video}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="">Gambar</label>
                            <input type="file" name="gambar">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <button type="submit" class="merah">SIMPAN</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- End Page Banner -->
@endsection
@section('js')

@endsection