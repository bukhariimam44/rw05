@extends('layouts.umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
<!-- include libraries(jQuery, bootstrap) -->
<link href="{{asset('summernote/css/bootstrap.min.css')}}" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<style>
.merah {
    border: none;
    background-color: #ff661f;
    color: #ffffff;
    width: 100%;
    height:50px;
    margin-top: 15px;
    border-radius: 5px;
    cursor: pointer;
    transition:0.5s;
}
</style>
@endsection
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h2>Tambah Berita</h2>
            <ul>
            <li><a href="{{route('home')}}">Home</a></li>
                <li>Tambah Berita</li>
            </ul>
        </div>
    </div>
</div>
<section class="news-area ptb-50">
    <div class="container">
        <div>
            <form method="post" action="{{route('add-berita')}}" enctype="multipart/form-data"id="upload">
            @csrf
            <input type="hidden" name="action" value="tambah">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="">Judul</label>
                            <input type="text" name="judul" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Kategori</label>
                            <select name="kategori" id="" class="form-control" required>
                            @foreach($kategories as $kategori)
                            <option value="{{$kategori->id}}">{{$kategori->kategori}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Gambar</label>
                            <input type="file" name="gambar" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="">Isi Berita</label>
                            <textarea id="summernote" name="isi" required></textarea>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <button type="button" onclick="myFunctionUpload()" class="merah">SIMPAN</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- End Page Banner -->
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script type="text/javascript">
					
    function myFunctionUpload() {
        //Loading
        Swal.fire({
                title: 'Mohon menunggu...',
                text:'Berita Sedang diupload..',
                allowEscapeKey: false,
                allowOutsideClick: false,
                background: '#FFFFFF',
                showConfirmButton: false,
                onOpen: ()=>{
                                Swal.showLoading();
                }
            }).then((dismiss) => {
                // Swal.showLoading();
                }
        );
        //END LOADING
        document.getElementById('upload').submit();
    }
</script>
<script>
    $('#summernote').summernote({
    placeholder: 'Isi Berita',
    tabsize: 2,
    height: 300,
    toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['fontname', ['fontname']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link', 'picture']],
          ['view', ['fullscreen', 'codeview', 'help']]
        ]
    });
</script>
@endsection