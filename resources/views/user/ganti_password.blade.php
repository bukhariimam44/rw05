@extends('layouts.umum')
@section('css')
<style>
.merah {
    border: none;
    background-color: #ff661f;
    color: #ffffff;
    width: 100%;
    height:50px;
    margin-top: 15px;
    border-radius: 5px;
    cursor: pointer;
    transition:0.5s;
}
select.form-control{
    margin-top:20px !important;
}
</style>
@endsection
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h2>Ganti Password</h2>
            <ul>
            <li><a href="{{route('home')}}">Home</a></li>
                <li>Ganti Password</li>
            </ul>
        </div>
    </div>
</div>
<section class="news-area ptb-50">
    <div class="container">
        <div class="login-form">
            <form method="post" action="{{route('ganti-password')}}">
            @csrf
                <div class="row">
                    <div class="form-group">
                        <label>Password Lama</label>
                        <input type="text" class="form-control" name="ps_lm" placeholder="Masukan Password Lama...">
                    </div>
                    <div class="form-group">
                        <label>Password Baru</label>
                        <input type="text" class="form-control" name="ps_br" placeholder="Masukan Password Baru...">
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <button type="submit" class="merah">SIMPAN</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- End Page Banner -->
@endsection
