@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h2>Data Uang Masuk</h2>
            <ul>
            <li><a href="{{route('home')}}">Home</a></li>
                <li>Data Uang Masuk</li>
            </ul>
        </div>
    </div>
</div>
        <!-- End Page Banner -->

        <section class="news-area ptb-50">
            <div class="container">
                <div class="row">
                <div class="col-md-12">
                    @if(Auth::check())
                    @if(Auth::user()->type == 'Admin')
                    <a href="{{route('tambah-uang-masuk')}}" class="btn btn-success"><i class='bx bx-plus-circle'></i> Tambah Data</a> <hr>
                    @endif
                    @endif
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <th>No.</th>
                                <th>No. Transaksi</th>
                                <th>Tgl. Transaksi</th>
                                <th>Nominal</th>
                                <th>Keterangan</th>
                                @if(Auth::check())
                                @if(Auth::user()->type == 'Admin')
                                <th width="150px">Action</th>
                                @endif
                                @endif
                            </thead>
                            <tbody>
                                @foreach($datas as $key => $data)
                                    <tr>
                                        <td>{{$key+1}}.</td>
                                        <td>{{$data->no_trx}}</td>
                                        <td>{{date('d M Y', strtotime($data->tgl_trx))}}</td>
                                        <td>{{number_format($data->nominal,0,',','.')}}</td>
                                        <td>{{$data->keterangan}}</td>
                                        @if(Auth::check())
                                        @if(Auth::user()->type == 'Admin')
                                        <td>
                                        <a href="{{route('edit-uang-masuk',$data->id)}}" class="btn btn-warning">Edit</a> 
                                        <a href="{{route('hapus-uang-masuk',$data->id)}}" class="btn btn-danger">Hapus</a> 
                                        </td>
                                        @endif
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                    
                </div>
            </div>
        </section>

@endsection