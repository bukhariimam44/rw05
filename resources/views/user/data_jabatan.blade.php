@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h2>Data Jabatan</h2>
            <ul>
            <li><a href="{{route('home')}}">Home</a></li>
                <li>Data Jabatan</li>
            </ul>
        </div>
    </div>
</div>
        <!-- End Page Banner -->

        <section class="news-area ptb-50">
            <div class="container">
                <div class="row">
                <div class="col-md-12">
                    @if(Auth::check())
                    @if(Auth::user()->type == 'Admin')
                    <a href="{{route('tambah-jabatan')}}" class="btn btn-success"><i class='bx bx-plus-circle'></i> Tambah Jabatan</a> <hr>
                    @endif
                    @endif
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <th>No.</th>
                                <th>Jabatan</th>
                                <th width="300px">Action</th>
                            </thead>
                            <tbody>
                                @foreach($datas as $key => $data)
                                    <tr>
                                        <td>{{$key+1}}.</td>
                                        <td>{{$data->jabatan}}</td>
                                        <td>
                                        <a href="{{route('edit-jabatan', $data->id)}}" class="btn btn-warning">Edit</a> 
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                    
                </div>
            </div>
        </section>

@endsection