@extends('layouts.umum')

@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
            <div class="container">
                <div class="page-title-content">
                    <h2>Kontak</h2>
                    <ul>
                        <li><a href="{{route('index')}}">Beranda</a></li>
                        <li>Kontak</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Page Banner -->

        <!-- Start Contact Area -->
        <section class="contact-area ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="contact-map">
                            {!! $datas->maps !!}
                            <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27982.0992479593!2d-81.35428553933833!3d28.75650994456714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88e76d5129fed6b1%3A0x1a6cd960f325cfcb!2sLake%20Mary%2C%20FL%2032746%2C%20USA!5e0!3m2!1sen!2sbd!4v1602575597158!5m2!1sen!2sbd"></iframe> -->
                        </div>

                        <ul class="contact-info">
                            <li>
                                <span>Alamat :</span> <br>
                                {{ $datas->alamat }}
                            </li>
                            <li>
                                <span>Telpon :</span>
                                <a href="tel:{{ $datas->telpon }}">{{ $datas->telpon }}</a>
                            </li>
                            <li>
                                <span>Email :</span>
                                <a href="mailto:{{ $datas->email }}">{{ $datas->email }}</a>
                            </li>
                            <li>
                                @if(Auth::check())
                                @if(Auth::user()->type == 'Admin')
                                <a href="{{route('edit-kontak')}}" class="btn btn-success text-white"><i class='bx bxs-edit'></i> Edit Kontak</a>
                                @endif
                                @endif
                            </li>
                        </ul>

                        <div class="contact-form">
                            <div class="title">
                                <h3>Siap memulai?</h3>
                                <p>Kirimkan Pesan Kepada Kami</p>
                            </div>

                            <form action="{{route('kontak')}}" method="post">
                            @csrf
                            <input type="hidden" name="action" value="pesan">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="nama" class="form-control" id="nama" required data-error="Masukan nama lengkap" placeholder="Nama Lengkap">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <input type="email" name="email" class="form-control" id="email" required data-error="Masukan Email" placeholder="Email">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="nohp" class="form-control" id="nohp" required data-error="Masukan Nomor HP" placeholder="Nomor HP">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="judul" class="form-control" id="judul" required data-error="Masukan Judul Pesan" placeholder="Judul Pesan">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <textarea name="pesan" id="pesan" class="form-control" cols="30" rows="6" required data-error="Masukan Isi Pesan" placeholder="Ketik pesan disini..."></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" data-error="Setujui Syarat dan Ketentuan." name="setujui" required id="setujui">
                                            <label class="form-check-label" for="checkme">
                                                Setujui, <a href="{{route('syarat-dan-ketentuan')}}">Syarat dan Ketentuan </a>pengiriman pesan. <br>
                                                <div class="help-block with-errors"></div>
                                            </label>
                                            
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12">
                                        <button type="submit" class="default-btn">Kirim Pesan</button>
                                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <aside class="widget-area">
                        @include('includes.logo')
                        @include('includes.sosmed')
                        @include('includes.berlangganan')
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Contact Area -->
        @endsection