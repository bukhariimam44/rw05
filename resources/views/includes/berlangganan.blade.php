<section class="widget widget_newsletter" id="berlangganan">
    <div class="newsletter-content">
        <h3>Berlangganan Berita</h3>
        <p>Berlangganan untuk mendapatkan berita terbaru dari kami.</p>
    </div>   

    <form class="newsletter-form" data-toggle="validator">
        <input type="email" class="input-newsletter" placeholder="Masukan Email Kamu" v-model="email_langganan" required autocomplete="off">
        <button type="button" @click="proses()">Berlangganan</button>
        <div id="validator-newsletter" class="form-result"></div>
    </form>
</section>
