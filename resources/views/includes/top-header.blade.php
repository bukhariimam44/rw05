<div class="top-header-area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <ul class="top-header-social">
                            <li>
                                <a href="#" class="facebook" target="_blank">
                                    <i class='bx bxl-facebook'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="pinterest" target="_blank">
                                    <i class='bx bxl-instagram'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="pinterest" target="_blank">
                                    <i class='bx bxl-linkedin'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="twitter" target="_blank">
                                    <i class='bx bxl-twitter'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="linkedin" target="_blank">
                                    <i class='bx bxl-youtube'></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-lg-6">
                        <ul class="top-header-others">
                            <li>
                                <div class="languages-list">
                                    <select>
                                        <option value="1">Indonesia</option>
                                        <option value="2">English</option>
                                    </select>
                                </div>
                            </li>

                            <li>
                            @if(Auth::check())
                                <i class='bx bx-lock'></i>
                                <a href="{{route('logout')}}">Logout</a>
                            @else
                                <i class='bx bx-user'></i>
                                <a href="{{route('login')}}">Login</a>
                            @endif
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>