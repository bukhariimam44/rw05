<ul class="navbar-nav">
                                <li class="nav-item">
                                    <a href="{{ route('index') }}" class="nav-link {{ setActive('index') }}">
                                        Beranda
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('tentang') }}" class="nav-link {{ setActive(['tentang','edit-tentang']) }}">
                                        Tentang
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('berita') }}" class="nav-link {{ setActive(['berita','detail-berita','edit-berita']) }}">
                                        Berita
                                    </a>
                                </li>
                                

                                <li class="nav-item">
                                    <a href="{{ route('video') }}" class="nav-link {{ setActive('video') }}">
                                        Video
                                    </a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a href="{{ route('pengurus') }}" class="nav-link {{ setActive('pengurus') }}">
                                        Pengurus
                                    </a>
                                </li> -->
                                <li class="nav-item">
                                    <a href="{{ route('struktur-organisasi') }}" class="nav-link {{ setActive('struktur-organisasi') }}">
                                        Struktuk Organisasi
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('kontak') }}" class="nav-link {{ setActive('kontak') }}">
                                        Kontak
                                    </a>
                                </li>
                                @if(Auth::check())
                                <li class="nav-item">
                                @if(Auth::user()->type == 'Admin')
                                    <a href="#" class="nav-link {{ setActive(['home','berita-pengajuan-warga']) }}">
                                        Admin 
                                        <i class='bx bx-chevron-down'></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        
                                        <li class="nav-item">
                                            <a href="{{route('berita-pengajuan-warga')}}" class="nav-link">
                                                Pengajuan Warga
                                            </a>
                                        </li>

                                        
                                    </ul>
                                    @else 
                                    <a href="#" class="nav-link">
                                        Warga 
                                        <i class='bx bx-chevron-down'></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        
                                        <li class="nav-item">
                                            <a href="" class="nav-link">
                                                Home one
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a href="" class="nav-link">
                                                Home two
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a href="" class="nav-link">
                                                Home three
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a href="" class="nav-link">
                                                Home four
                                            </a>
                                        </li>
                                    </ul>
                                    @endif
                                </li>
                                @endif
                            </ul>