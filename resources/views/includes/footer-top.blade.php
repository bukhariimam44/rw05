<section class="footer-area pt-100 pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="single-footer-widget">
                            <a href="#">
                                <img src="{{asset('assets/gambar/tci2.jpg')}}" alt="image">
                            </a>
                            <p>Media Informasi Warga RW 05 Taman Cilegon Indah</p>

                            <ul class="social">
                                <li>
                                    <a href="#" class="facebook" target="_blank">
                                        <i class='bx bxl-facebook'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="twitter" target="_blank">
                                        <i class='bx bxl-instagram'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="linkedin" target="_blank">
                                        <i class='bx bxl-twitter'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="linkedin" target="_blank">
                                        <i class='bx bxl-youtube'></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="single-footer-widget">
                            <h2>Berita Terbaru</h2>
                            @foreach(App\Berita::where('dari','Admin')->where('open','=',1)->orderBy('id','DESC')->paginate(3) as $data)
                            <div class="post-content">
                                <div class="row align-items-center">
                                    <div class="col-md-4">
                                        <div class="post-image">
                                            <a href="{{route('detail-berita',str_replace(' ','_',$data->judul))}}">
                                                <img src="{{asset('assets/gambar/berita/'.$data->gambar)}}" alt="image">
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <h4>
                                            <a href="{{route('detail-berita',str_replace(' ','_',$data->judul))}}">{{ substr($data->judul, 0, 30) }} ...</a>
                                        </h4>
                                        <span>{{date('d M, Y', strtotime($data->created_at))}}</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <!-- <div class="post-content">
                                <div class="row align-items-center">
                                    <div class="col-md-4">
                                        <div class="post-image">
                                            <a href="#">
                                                <img src="{{asset('assets/img/recent-post/recent-post-2.jpg')}}" alt="image">
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <h4>
                                            <a href="#">The match of the volleyball full of excitement</a>
                                        </h4>
                                        <span>28 Sep 2021</span>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="single-footer-widget">
                            <h2>Menu</h2>

                            <ul class="useful-links-list">
                                <li>
                                    <a href="{{route('index')}}">Beranda</a>
                                </li>
                                <li>
                                    <a href="{{route('tentang')}}">Tentang</a>
                                </li>
                                <li>
                                    <a href="{{route('berita')}}">Berita</a>
                                </li>
                                <li>
                                    <a href="{{route('video')}}">Video</a>
                                </li>
                                <li>
                                    <a href="{{route('struktur-organisasi')}}">Struktur Organisasi</a>
                                </li>
                                <li>
                                    <a href="{{route('kontak')}}">Kontak</a>
                                </li>
                                <li>
                                    <a href="{{route('syarat-dan-ketentuan')}}">Syarat dan Ketentuan</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6" id="langganan">
                        <div class="single-footer-widget">
                            <h2>Berlangganan Berita</h2>

                            <div class="widget-subscribe-content">
                                <p>Berlangganan untuk mendapatkan berita terbaru dari kami.</p>

                                <form class="newsletter-form">
                                    <input type="email" class="input-newsletter" placeholder="Masukan Email Kamu" v-model="email_langganan2" required>
            
                                    <button type="button" @click="proseses()">Berlangganan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>