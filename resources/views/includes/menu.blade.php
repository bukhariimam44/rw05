<ul class="navbar-nav">
                                <li class="nav-item">
                                    <a href="{{ route('index') }}" class="nav-link {{ setActive('index') }}">
                                        Beranda
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('tentang') }}" class="nav-link {{ setActive(['tentang','edit-tentang']) }}">
                                        Tentang
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('berita') }}" class="nav-link {{ setActive(['berita','detail-berita','edit-berita','tambah-berita']) }}">
                                        Berita
                                    </a>
                                </li>
                                

                                <li class="nav-item">
                                    <a href="{{ route('video') }}" class="nav-link {{ setActive('video') }}">
                                        Video
                                    </a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a href="{{ route('pengurus') }}" class="nav-link {{ setActive('pengurus') }}">
                                        Pengurus
                                    </a>
                                </li> -->
                                <li class="nav-item">
                                    <a href="{{ route('struktur-organisasi') }}" class="nav-link {{ setActive('struktur-organisasi') }}">
                                        Struktuk Organisasi
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('kontak') }}" class="nav-link {{ setActive('kontak') }}">
                                        Kontak
                                    </a>
                                </li>
                                @if(Auth::check())
                                <li class="nav-item">
                                @if(Auth::user()->type == 'Admin')
                                    <a href="#" class="nav-link {{ setActive(['data-jabatan','edit-jabatan','tambah-jabatan','home','data-user','berita-pengajuan-warga','data-uang-masuk','data-uang-keluar','mutasi-keuangan','laporan-keuangan','data-komentar']) }}">
                                        Admin 
                                        <i class='bx bx-chevron-down'></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                    <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                Data Master 
                                                <i class='bx bx-chevron-down'></i>
                                            </a>
        
                                            <ul class="dropdown-menu">
                                                <li class="nav-item">
                                                    <a href="{{route('data-jabatan')}}" class="nav-link">
                                                        Data Jabatan
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                Keuangan 
                                                <i class='bx bx-chevron-down'></i>
                                            </a>
        
                                            <ul class="dropdown-menu">
                                                <li class="nav-item">
                                                    <a href="{{route('data-uang-masuk')}}" class="nav-link">
                                                        Data Uang Masuk
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="{{route('data-uang-keluar')}}" class="nav-link">
                                                        Data Uang Keluar
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="{{route('mutasi-keuangan')}}" class="nav-link">
                                                        Mutasi Keuangan 
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="{{route('laporan-keuangan')}}" class="nav-link">
                                                        Laporan Keuangan 
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{route('data-user')}}" class="nav-link">
                                                Data User
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{route('berita-pengajuan-warga')}}" class="nav-link">
                                                Berita Pengajuan Warga
                                            </a>
                                        </li>
                                        
                                        <li class="nav-item">
                                            <a href="{{route('data-komentar')}}" class="nav-link">
                                                Data Komentar
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{route('ganti-password')}}" class="nav-link">
                                                Ganti Password
                                            </a>
                                        </li>
                                    </ul>
                                    @else 
                                    <a href="#" class="nav-link {{ setActive(['home','add-berita','data-uang-masuk','data-uang-keluar','mutasi-keuangan','laporan-keuangan'])}}">
                                        Warga 
                                        <i class='bx bx-chevron-down'></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="{{route('add-berita')}}" class="nav-link">
                                                Tambah Berita
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                Keuangan 
                                                <i class='bx bx-chevron-down'></i>
                                            </a>
        
                                            <ul class="dropdown-menu">
                                                <li class="nav-item">
                                                    <a href="{{route('data-uang-masuk')}}" class="nav-link">
                                                        Data Uang Masuk
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="{{route('data-uang-keluar')}}" class="nav-link">
                                                        Data Uang Keluar
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="{{route('mutasi-keuangan')}}" class="nav-link">
                                                        Mutasi Keuangan 
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="{{route('laporan-keuangan')}}" class="nav-link">
                                                        Laporan Keuangan 
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-item">
                                            <a href="{{route('ganti-password')}}" class="nav-link">
                                                Ganti Password
                                            </a>
                                        </li>
                                    </ul>
                                    @endif
                                </li>
                                @endif
                            </ul>