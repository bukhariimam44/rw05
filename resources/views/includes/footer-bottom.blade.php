@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
<div class="copyright-area">
            <div class="container">
                <div class="copyright-area-content">
                    <p>
                        Copyright © 2021 Taman Cilegon Indah RW05 - All Rights Reserved <br>
                        Create By <a href="https://wa.me/6282312543008" target="_blank">Imam Bukhari</a>
                    </p>
                </div>
            </div>
        </div>
@section('js')
<?php $ppp = $_SERVER['REQUEST_URI'];?>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
@if($ppp == '/' || $ppp == '/berita'|| $ppp == '/video'|| $ppp == '/kontak')
<script>
    var berlangganan = new Vue({
        el:'#berlangganan',
        data:{
            email_langganan:'',
            reg: new RegExp(/^[a-z0-9](\.?[a-z0-9]){5,}@gmail\.com$/)
        }, 
        methods:{
            loadingMenunggu(text){
                Swal.fire({
                    title: text,
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    background: '#FFFFFF',
                    showConfirmButton: false,
                    onOpen: ()=>{
                                    Swal.showLoading();
                    }
                    }).then((dismiss) => {
                        // Swal.showLoading();
                    });
            },
            async proses(){
                if (this.email_langganan.length < 1) {
                    Swal.fire({
                    title: 'Oops!',
                    text: 'Gmail harus diisi',
                    icon: 'error',
                    showConfirmButton: false,
                    timerProgressBar: true,
                    timer: 3000
                    })
                } else if (!this.reg.test(this.email_langganan)) {
                    Swal.fire({
                    title: 'Oops!',
                    text: 'Gmail tidak falid',
                    icon: 'error',
                    showConfirmButton: false,
                    timerProgressBar: true,
                    timer: 3000
                    })
                }else{
                    this.loadingMenunggu('Mohon menunggu...');
                    let url = "<?php echo route('berlangganan');?>";
                    let request = {email : this.email_langganan};
                    request['token'] = document.querySelector('#token').getAttribute('value');
                        await axios.post(url,request).then((response) =>{
                        console.log('Res Captcha'+JSON.stringify(response.data));
                        Swal.fire({
                            icon: response.data.icon,
                            title: response.data.title,
                            text: response.data.message
                        })
                    },(response)=>{
                        // console.log('ERROR: '+response);
                        Swal.fire({icon:"error!", text:response, title:"Gagal"});
                        Swal.close()
                    }); 
                }
                    
            },
        }
    });
</script>
@endif
<script>
    var langganan = new Vue({
    el:'#langganan',
    data:{
        email_langganan2:'',
        reg: new RegExp(/^[a-z0-9](\.?[a-z0-9]){5,}@gmail\.com$/)
    }, 
    methods:{
        loadingMenunggu(text){
            Swal.fire({
                title: text,
                allowEscapeKey: false,
                allowOutsideClick: false,
                background: '#FFFFFF',
                showConfirmButton: false,
                onOpen: ()=>{
                                Swal.showLoading();
                }
                }).then((dismiss) => {
                    // Swal.showLoading();
                });
        },
        async proseses(){
            if (this.email_langganan2.length < 1) {
                Swal.fire({
                title: 'Oops!',
                text: 'Gmail harus diisi',
                icon: 'error',
                showConfirmButton: false,
                timerProgressBar: true,
                timer: 3000
                })
            } else if (!this.reg.test(this.email_langganan2)) {
                Swal.fire({
                title: 'Oops!',
                text: 'Gmail tidak falid',
                icon: 'error',
                showConfirmButton: false,
                timerProgressBar: true,
                timer: 3000
                })
            }else{
                this.loadingMenunggu('Mohon menunggu...');
                let url = "<?php echo route('berlangganan');?>";
                let request = {email : this.email_langganan2};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(url,request).then((response) =>{
                    console.log('Res Captcha'+JSON.stringify(response.data));
                    Swal.fire({
                        icon: response.data.icon,
                        title: response.data.title,
                        text: response.data.message
                    })
                },(response)=>{
                    // console.log('ERROR: '+response);
                    Swal.fire({icon:"error!", text:response, title:"Gagal"});
                    Swal.close()
                }); 
            }
                
        },
    }
    });
</script>
@if($ppp == '/login')
<script>
    var login = new Vue({
    el:'#login',
    data:{
        email:'',
        password:'',
        captcha:'',
        key:'',
        imgCaptcha:'',
        remember:false
    },
    mounted(){
        this.getCaptcha();
    },
    methods:{
        loadingMenunggu(text){
            Swal.fire({
                title: text,
                allowEscapeKey: false,
                allowOutsideClick: false,
                background: '#FFFFFF',
                showConfirmButton: false,
                onOpen: ()=>{
                                Swal.showLoading();
                }
                }).then((dismiss) => {
                    // Swal.showLoading();
                });
        },
        async getCaptcha(){
            let urlCaptcha = "<?php echo url('captcha/api/math');?>";
            await axios.get(urlCaptcha).then((response) =>{
                // console.log('Res Captcha'+JSON.stringify(response.data));
                this.key = response.data.key
                this.imgCaptcha = response.data.img
                
            },(response)=>{
                // console.log('ERROR: '+response);
                Swal.fire({icon:"error!", text:response, title:"Gagal"});
                Swal.close()
            }); 
        },
        remember_me(){
            if (this.remember == false) {
                this.remember = true;
            }else{
                this.remember = false;
            }
        },
        async login(){
            if (this.email && this.password) {
                this.loadingMenunggu('Mohon menunggu...');
                let url_login = "<?php echo route('login');?>";
                let request = {email : this.email, password: this.password, remember:this.remember, key:this.key, captcha: this.captcha};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(url_login,request).then((response) =>{
                    // console.log('Res Login'+JSON.stringify(response.data));
                    this.getCaptcha();
                    Swal.fire({
                        icon: response.data.icon,
                        title: response.data.title,
                        text: response.data.message
                    })
                    if (response.data.code === 200) {
                        window.location.href = "<?php echo route('home'); ?>";
                    }
                    Swal.close()
                },(response)=>{
                    // console.log('ERROR: '+response);
                    Swal.fire({icon:"error!", text:response, title:"Gagal"});
                    Swal.close()
                });
                
            }else{
                this.getCaptcha();
                Swal.fire({icon:"error!", text:"Email atau Password Salah", title:"Gagal"});
            }
            
        }
    }
})
</script>
@endif
@if($ppp == '/daftar')
<script>
var daftar = new Vue({
    el:'#daftar',
    data:{
        name:'',
        email:'',
        nohp:'',
        rt:'',
        password:'',
        captcha:'',
        key:'',
        imgCaptcha:'',
        reg: new RegExp(/^[a-z0-9](\.?[a-z0-9]){5,}@gmail\.com$/)
    },
    mounted(){
        this.getCaptcha();
    },
    methods:{
        loadingMenunggu(text){
            Swal.fire({
                title: text,
                allowEscapeKey: false,
                allowOutsideClick: false,
                background: '#FFFFFF',
                showConfirmButton: false,
                onOpen: ()=>{
                                Swal.showLoading();
                }
                }).then((dismiss) => {
                    // Swal.showLoading();
                });
        },
        async getCaptcha(){
            let urlCaptcha = "<?php echo url('captcha/api/math');?>";
            await axios.get(urlCaptcha).then((response) =>{
                // console.log('Res Captcha'+JSON.stringify(response.data));
                this.key = response.data.key
                this.imgCaptcha = response.data.img
                
            },(response)=>{
                // console.log('ERROR: '+response);
                Swal.fire({icon:"error!", text:response, title:"Gagal"});
                Swal.close()
            }); 
        },
        async daftar(){
            if (this.name.length < 3) {
                Swal.fire(
                'Oops!',
                'Nama minimal 3 huruf',
                'error'
                )
            
            } else if (this.email.length < 1) {
                Swal.fire({
                title: 'Oops!',
                text: 'Gmail harus diisi',
                icon: 'error',
                showConfirmButton: false,
                timerProgressBar: true,
                timer: 3000
                })
            } else if (!this.reg.test(this.email)) {
                Swal.fire({
                title: 'Oops!',
                text: 'Gmail tidak falid',
                icon: 'error',
                showConfirmButton: false,
                timerProgressBar: true,
                timer: 3000
                })
            } else if (this.nohp.length < 10) {
                Swal.fire({
                title: 'Oops!',
                text: 'Nomor HP minimal 10 karakter',
                icon: 'error',
                showConfirmButton: false,
                timerProgressBar: true,
                timer: 3000
                })
            } else if (this.rt.length < 2) {
                Swal.fire({
                title: 'Oops!',
                text: 'RT minimal 2 karakter',
                icon: 'error',
                showConfirmButton: false,
                timerProgressBar: true,
                timer: 3000
                })
            } else if (this.password.length < 6) {
                Swal.fire({
                title: 'Oops!',
                text: 'Katasandi minimal 6 karakter',
                icon: 'error',
                showConfirmButton: false,
                timerProgressBar: true,
                timer: 3000
                })
            } else if (this.captcha.length < 1) {
                Swal.fire({
                title: 'Oops!',
                text: 'Hasilnya harus diisi',
                icon: 'error',
                showConfirmButton: false,
                timerProgressBar: true,
                timer: 3000
                })
            }else{
                if (this.name && this.email && this.nohp && this.rt && this.password && this.key && this.captcha) {
                    this.loadingMenunggu('Mohon menunggu...');
                    let url = "<?php echo route('postdaftar');?>";
                    let request = {name:this.name, email : this.email, nohp:this.nohp, rt:this.rt, password: this.password, key:this.key, captcha: this.captcha};
                    request['token'] = document.querySelector('#token').getAttribute('value');
                    await axios.post(url,request).then((response) =>{
                        console.log('Res DAFTAR'+JSON.stringify(response.data));
                        this.getCaptcha();
                        Swal.fire({
                            icon: response.data.icon,
                            title: response.data.title,
                            text: response.data.message
                        });
                        if (response.data.code === 200) {
                            window.location.href = "<?php echo route('login'); ?>";
                        }
                        Swal.close()
                    },(response)=>{
                        // console.log('ERROR: '+response);
                        Swal.fire({icon:"error!", text:response, title:"Gagal"});
                        Swal.close()
                    });
                    
                }else{
                    this.getCaptcha();
                    Swal.fire({icon:"error!", text:"Isi semua data dengan benar", title:"Gagal"});
                }
            }
            
        }
    }
})
</script>
@endif
    @endsection