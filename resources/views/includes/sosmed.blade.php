<section class="widget widget_stay_connected">
                                <h3 class="widget-title">Tetap Terhubung</h3>
                                
                                <ul class="stay-connected-list">
                                    <li>
                                        <a href="#">
                                            <i class='bx bxl-facebook'></i>
                                            120,345 Fans
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="twitter">
                                            <i class='bx bxl-twitter'></i>
                                            25,321 Followers
                                        </a>
                                    </li>

                                   

                                    <li>
                                        <a href="#" class="youtube">
                                            <i class='bx bxl-youtube'></i>
                                            101,545 Subscribers
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="instagram">
                                            <i class='bx bxl-instagram'></i>
                                            10,129 Followers
                                        </a>
                                    </li>

                                   
                                </ul>
                            </section>