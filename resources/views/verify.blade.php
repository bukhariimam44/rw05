@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h2>Verifikasi</h2>
            <ul>
                <li><a href="index.html">Home</a></li>
                <li>Verifikasi</li>
            </ul>
        </div>
    </div>
</div>
        <!-- End Page Banner -->

        <section class="login-area ptb-50" id="login">
            <div class="container">
                <div class="login-form">
                <h2>Akun anda masih menunggu persetujuan dari admin.</h2>
                </div>
            </div>
        </section>

@endsection