@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h2>Syarat dan Ketentuan</h2>
            <ul>
                <li><a href="{{route('home')}}">Home</a></li>
                <li>Syarat dan Ketentuan</li>
            </ul>
        </div>
    </div>
</div>
        <!-- End Page Banner -->

        <section class="news-area ptb-50">
            <div class="container">
                <div class="row">
                <div class="col-md-12">
                @if(Auth::check())
                @if(Auth::user()->type == 'Admin')
                <a href="{{route('tambah-syarat-dan-ketentuan')}}" class="btn btn-success"><i class='bx bx-plus-circle'></i> Tambah</a>
                @endif
                @endif
                <br><br>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <th>No.</th>
                                <th>Syarat dan Ketentuan</th>
                                @if(Auth::check())
                                @if(Auth::user()->type == 'Admin')
                                <th width="100px">Action</th>
                                @endif
                                @endif
                            </thead>
                            <tbody>
                                @foreach($datas as $key => $data)
                                    <tr>
                                        <td>{{$key+1}}.</td>
                                        <td>{{$data->syarat}}</td>
                                        @if(Auth::check())
                                        @if(Auth::user()->type == 'Admin')
                                        <td>
                                        <a href="{{route('edit-syarat-dan-ketentuan',$data->id)}}" class="btn btn-warning">Edit</a> <br><br>
                                        <a href="{{route('hapus-syarat-dan-ketentuan',$data->id)}}" class="btn btn-danger">Hapus</a> 
                                        </td>
                                        @endif
                                        @endif
                                    </tr>
                                @endforeach
                                @if(count($datas) < 1)
                                <tr>
                                    @if(Auth::check())
                                        @if(Auth::user()->type == 'Admin')
                                        <td align="center" colspan="3">Data belum ada</td>
                                        @else
                                        <td align="center" colspan="2">Data belum ada</td>
                                        @endif

                                    @else
                                    <td align="center" colspan="2">Data belum ada</td>
                                    @endif
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                    
                </div>
            </div>
        </section>

@endsection