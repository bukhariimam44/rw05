@extends('layouts.umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
<style>
.merah {
    border: none;
    background-color: #ff661f;
    color: #ffffff;
    width: 100%;
    height:50px;
    margin-top: 15px;
    border-radius: 5px;
    cursor: pointer;
    transition:0.5s;
}
</style>
@endsection
@section('content')
 <!-- Start Page Banner -->
 <div class="page-title-area">
            <div class="container">
                <div class="page-title-content">
                    <h2>Login</h2>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li>Login</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Page Banner -->

        <!-- Start Login Area -->
        <section class="login-area ptb-50" id="login">
            <div class="container">
                <div class="login-form">
                <h2>Khusus Warga RW 05</h2>

                    <form>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" v-model="email" class="form-control" placeholder="Email">
                        </div>

                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" v-model="password" class="form-control" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <img :src="imgCaptcha" alt="">
                            <input type="text" v-model="captcha" placeholder="Hasilnya" style="height:37px;">
                        </div>
                        <div class="row align-items-center">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-check">
                                    <input type="checkbox" @click="remember_me()" class="form-check-input" id="checkme">
                                    <label class="form-check-label" for="checkme">Tetap Login</label>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 lost-your-password">
                                <a href="#" class="lost-your-password">Lupa password?</a>
                            </div>
                        </div>

                        <button @click="login()" class="merah" type="button">Login</button>
                    </form>

                    <div class="important-text">
                        <p>Belum punya akun? <a href="{{route('daftar')}}">Daftar Sekarang!</a></p>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Login Area -->
@endsection
