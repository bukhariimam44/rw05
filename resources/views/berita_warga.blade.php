@extends('layouts.umum')
@section('css')
<style>
.hilang{
    display: none;
}
</style>
@endsection
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
            <div class="container">
                <div class="page-title-content">
                    <h2>Berita</h2>
                    <ul>
                        <li><a href="{{route('index')}}">Beranda</a></li>
                        <li>Berita</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Page Banner -->

        <!-- Start News Area -->
        <section class="news-area ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                    @if(Auth::check())
                    @if(Auth::user()->type == 'Admin')
                    <a href="{{route('tambah-berita')}}" class="btn btn-success"><i class='bx bx-plus-circle'></i> Tambah Berita</a> <hr>
                    @endif
                    @endif
                    @foreach($datas as $key=> $data)
                        <div class="single-news-item">
                            <div class="row align-items-center">
                                <div class="col-lg-4">
                                    <div class="news-image">
                                        <a href="{{route('detail-berita',str_replace(' ','_',$data->judul))}}">
                                            <img src="{{asset('assets/gambar/berita/'.$data->gambar)}}" alt="image">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-lg-8">
                                    <div class="news-content">
                                        <span>{{$data->KategoriId->kategori}}</span>
                                        <h3>
                                            <a href="{{route('detail-berita',str_replace(' ','_',$data->judul))}}">{{$data->judul}}</a>
                                        </h3>
                                        <!-- <p>{!! substr($data->isi, 0, 130) !!} ...</p> -->
                                        <p><a href="{{route('detail-berita',str_replace(' ','_',$data->judul))}}">{{$data->UserId->name}}</a> / {{date('d M, Y', strtotime($data->created_at))}}</p>
                                        @if(Auth::check())
                                        @if(Auth::user()->type == 'Admin')
                                        <a href="{{route('edit-berita',str_replace(' ','_',$data->judul))}}" class="btn btn-warning"><i class='bx bxs-edit'></i> Edit</a> <a href="{{route('hapus-berita',$data->id)}}" class="btn btn-danger"><i class='bx bxs-trash'></i> Hapus</a>
                                        @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @if ($paginator->lastPage() > 1)
                        <div class="pagination-area">
                            <a href="{{ $paginator->url(1) }}" class="{{ ($paginator->currentPage() > 1) ? ' prev page-numbers' : 'hilang'}}">
                                <i class='bx bx-chevron-left'></i>
                            </a>
                            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                            <a href="{{ $paginator->url($i) }}" class="page-numbers {{ ($paginator->currentPage() == $i) ? ' current' : '' }}">{{ $i }}</a>
                            @endfor
                            <a href="{{ $paginator->url($paginator->currentPage()+1) }}" class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' hilang' : 'next page-numbers' }}">
                                <i class='bx bx-chevron-right'></i>
                            </a>
                        </div>
                        @endif
                    </div>

                    <div class="col-lg-4">
                        <aside class="widget-area">
                            <section class="widget widget_latest_news_thumb">
                                <h3 class="widget-title">Berita Pengajuan Warga</h3>
                                @foreach($berita_wargas as $berita_warga)
                                <article class="item">
                                    <a href="#" class="thumb">
                                    <img class="fullimage cover" src="{{asset('assets/gambar/berita/'.$berita_warga->gambar)}}" alt="">
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="{{route('detail-berita',str_replace(' ','_',$berita_warga->judul))}}">{{ substr($berita_warga->judul, 0, 40) }}..</a></h4>
                                        <span>{{date('d M Y', strtotime($berita_warga->created_at))}}</span>
                                    </div>
                                </article>
                                @endforeach
                                @if(count($berita_wargas) < 1)
                                <article class="item">
                                    <a href="#" class="thumb">
                                    <img class="fullimage cover" src="{{asset('assets/gambar/berita/download.jpeg')}}" alt="">
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="#">Belum ada berita</a></h4>
                                        <span>## ####, ####</span>
                                    </div>
                                </article>
                                @endif
                            </section>

                            @include('includes.logo')

                            @include('includes.sosmed')

                            @include('includes.berlangganan')
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <!-- End News Area -->
@endsection
