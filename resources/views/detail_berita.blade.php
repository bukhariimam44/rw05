@extends('layouts.umum')
@section('css')
<meta id="token" value="{{csrf_token()}}">
@endsection
@section('content')
 <!-- Start Page Banner -->
 <div class="page-title-area">
            <div class="container">
                <div class="page-title-content">
                    <h2>Detail Berita</h2>
                    <ul>
                        <li><a href="{{route('index')}}">Beranda</a></li>
                        <li>Detail Berita</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Page Banner -->

        <!-- Start News Details Area -->
        <section class="news-details-area ptb-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <div class="blog-details-desc">
                            <div class="article-image">
                                <img src="{{asset('assets/gambar/berita/'.$datas->gambar)}}" alt="image">
                            </div>

                            <div class="article-content">
                                <span><a href="#">{{$datas->UserId->name}}</a> / {{date('d M Y', strtotime($datas->created_at))}} / <a href="#">0 Komentar</a></span>
                                <h3>{{$datas->judul}}</h3>

                                <p>{!! $datas->isi !!}</h3>
                                

                                
                            </div>

                            <div class="article-footer">
                                <div class="article-share">
                                    <ul class="social">
                                        <li><span>Bagikan :</span></li>
                                        <li>
                                            <a href="#" target="_blank">
                                                <i class='bx bxl-facebook'></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank">
                                                <i class='bx bxl-twitter'></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" target="_blank">
                                                <i class='bx bxl-instagram'></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="post-navigation">
                                <div class="navigation-links">
                                
                                    <div class="nav-previous">
                                    @if($sebelum !== "kosong")
                                        <a href="{{route('detail-berita',str_replace(' ','_',$sebelum->judul))}}">
                                            <i class='bx bx-chevron-left'></i>
                                            Prev Post
                                        </a>
                                    @endif
                                    </div>
                                
                                
                                    <div class="nav-next">
                                    @if($sesudah !== "kosong")
                                        <a href="{{route('detail-berita',str_replace(' ','_',$sesudah->judul))}}">
                                            Next Post 
                                            <i class='bx bx-chevron-right'></i>
                                        </a>
                                    @endif
                                    </div>
                                
                                </div>
                            </div>

                            <div class="comments-area" id="komentar">
                                <h3 class="comments-title">@{{totKometar}} Komentar:</h3>

                                <ol class="comment-list">
                                    <li class="comment" v-for="(data, index) in komentars" :key="index">
                                        <div class="comment-body">
                                            <footer class="comment-meta">
                                                <div class="comment-author vcard">
                                                    <img src="{{asset('assets/gambar/user.png')}}" class="avatar" alt="image">
                                                    <b class="fn">@{{data.nama}}</b>
                                                </div>
                                                <div class="comment-metadata">
                                                    <a href="index.html">
                                                        <span>@{{formatDate(data.created_at)}}</span>
                                                    </a>
                                                </div>
                                            </footer>
                                            <div class="comment-content">
                                                <p>@{{data.komentar}}</p>
                                            </div>
                                            <!-- <div class="reply">
                                                <a href="#" class="comment-reply-link">Reply</a>
                                            </div> -->
                                        </div>
                                    </li>                                    
                                </ol>

                                <div class="comment-respond">
                                    <h3 class="comment-reply-title">Tinggalkan Komentar</h3>

                                    <form class="comment-form">
                                        <p class="comment-notes">
                                            <span id="email-notes">Khusus warga Taman Cilegon Indah RW.05 
                                        </p>
                                        <p class="comment-form-author">
                                            <label>Nama Lengkap</label>
                                            <input type="text" v-model="nama" placeholder="Nama Lengkap" required="required">
                                        </p>
                                        <p class="comment-form-email">
                                            <label>Email</label>
                                            <input type="email" v-model="email" placeholder="Email kamu" required="required">
                                        </p>
                                        <p class="comment-form-url">
                                            <label>No HP/WA</label>
                                            <input type="text" v-model="nohp" placeholder="Website">
                                        </p>
                                        <p class="comment-form-comment">
                                            <label>Isi Komentar</label>
                                            <textarea v-model="komentar" id="komentar" cols="45" placeholder="Komentar kamu." rows="5" maxlength="65525"></textarea>
                                        </p>
                                        <p class="comment-form-cookies-consent">
                                            <input type="checkbox" value="yes" name="wp-comment-cookies-consent" id="wp-comment-cookies-consent">
                                            <label for="wp-comment-cookies-consent">Setujui, <a href="{{route('syarat-dan-ketentuan')}}" style="color:#ff661f">Syarat dan Ketentuan </a>berkomentar.</label>
                                        </p>
                                        <p class="form-submit">
                                            <input type="button" id="submit" @click="simpanKomentar()" class="submit" value="Kirim Komentar">
                                        </p>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <aside class="widget-area">
                            <section class="widget widget_latest_news_thumb">
                                <h3 class="widget-title">Berita Terbaru</h3>
                                @foreach($beritas as $berita)
                                <article class="item">
                                    <a href="#" class="thumb">
                                    <img class="fullimage cover" src="{{asset('assets/gambar/berita/'.$berita->gambar)}}" alt="">
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="{{route('detail-berita',str_replace(' ','_',$berita->judul))}}">{{ substr($berita->judul, 0, 40) }}..</a></h4>
                                        <span>{{date('d M Y', strtotime($berita->created_at))}}</span>
                                    </div>
                                </article>
                                @endforeach
                                @if(count($beritas) < 1)
                                <article class="item">
                                    <a href="#" class="thumb">
                                        <span class="fullimage cover bg1" role="img"></span>
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="#">Belum ada berita</a></h4>
                                        <span>## ####, ####</span>
                                    </div>
                                </article>
                                @endif
                            </section>

                            <section class="widget widget_popular_posts_thumb">
                                <h3 class="widget-title">Berita Pengajuan Warga</h3>
                                @foreach($berita_wargas as $berita_warga)
                                <article class="item">
                                    <a href="#" class="thumb">
                                    <img class="fullimage cover" src="{{asset('assets/gambar/berita/'.$berita_warga->gambar)}}" alt="">
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="{{route('detail-berita',str_replace(' ','_',$berita_warga->judul))}}">{{ substr($berita_warga->judul, 0, 40) }}..</a></h4>
                                        <span>{{date('d M Y', strtotime($berita_warga->created_at))}}</span>
                                    </div>
                                </article>
                                @endforeach
                                @if(count($berita_wargas) < 1)
                                <article class="item">
                                    <a href="#" class="thumb">
                                    <img class="fullimage cover" src="{{asset('assets/gambar/berita/download.jpeg')}}" alt="">
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="#">Belum ada berita</a></h4>
                                        <span>## ####, ####</span>
                                    </div>
                                </article>
                                @endif
                            </section>

                            @include('includes.sosmed')
                            @include('includes.logo')
                            @include('includes.berlangganan')

                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <!-- Start News Details Area -->
@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script src="https://momentjs.com/downloads/moment.js"></script>
<script>
var komentar = new Vue({
    el:'#komentar',
    data:{
        ids:{{$datas->id}},
        komentars:[],
        totKometar:0,
        nama: '',
        email: '',
        nohp: '',
        komentar:''
    },
    mounted(){
        this.load();
    },
    methods:{
        formatDate(testDate){
            return moment(testDate).format('DD MMMM YYYY HH:II');
        },
        async load(){
            let url = "<?php echo route('komentar',$datas->id);?>";
            await axios.get(url).then((response) =>{
                console.log('Res Komentar'+JSON.stringify(response.data));
                this.komentars = response.data.data
                this.totKometar = response.data.total
                this.nama = response.data.user.name
                this.email = response.data.user.email
                this.nohp = response.data.user.nohp
                
            },(response)=>{
                // console.log('ERROR: '+response);
                swal("Gagal!", response, "error");
                Swal.close()
            }); 
        },
        async simpanKomentar(){
            if (this.ids && this.nama && this.email && this.nohp && this.komentar) {
                let url = "<?php echo route('postkomentar');?>";
                let request = {nama:this.nama, email : this.email, nohp: this.nohp, komentar:this.komentar, ids:this.ids};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(url,request).then((response) =>{
                    console.log('Res Login'+JSON.stringify(response.data));
                    this.load();
                    swal(response.data.title, response.data.message, response.data.icon);
                    // if (response.data.code === 200) {
                    //     window.location.href = "<?php echo route('home'); ?>";
                    // }
                    this.komentar = ''
                    Swal.close()
                },(response)=>{
                    // console.log('ERROR: '+response);
                    swal("Gagal!", response, "error");
                    Swal.close()
                });
                
            }else{
                this.getCaptcha();
                swal("Gagal!", "Email atau Password Salah", "error");
            }
        }
    }
})
</script>
@endsection