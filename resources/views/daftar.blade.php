@extends('layouts.umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
<style>
.merah {
    border: none;
    background-color: #ff661f;
    color: #ffffff;
    width: 100%;
    height:50px;
    margin-top: 15px;
    border-radius: 5px;
    cursor: pointer;
    transition:0.5s;
}
</style>
@endsection
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
            <div class="container">
                <div class="page-title-content">
                    <h2>Daftar</h2>
                    <ul>
                        <li><a href="{{route('index')}}">Beranda</a></li>
                        <li>Daftar</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Page Banner -->

        <!-- Start Register Area -->
        <section class="register-area ptb-50">
            <div class="container">
                <div class="register-form">
                    <h2>Khusus Warga RW 05</h2>

                    <form id="daftar">
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input type="text" class="form-control" v-model="name" placeholder="Nama Lengkap...">
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" v-model="email" placeholder="Email...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nomor HP</label>
                                    <input type="text" v-model="nohp" class="form-control" placeholder="Nomor HP...">
                                </div>
                            </div>
                        </div>
                                                
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>RT</label>
                                    <input type="text" v-model="rt" class="form-control" placeholder="RT...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>RW</label>
                                    <input type="text" value="05" class="form-control"  disabled>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" v-model="password" placeholder="Password...">
                        </div>

                        <div class="form-group">
                            <img :src="imgCaptcha" alt="">
                            <input type="text" v-model="captcha" placeholder="Hasilnya" style="height:37px;">
                        </div>
                        <button @click="daftar()" class="merah" type="button">Daftar Sekarang</button>
                    </form>

                    <div class="important-text">
                        <p>Sudah punya akun? <a href="{{route('login')}}">Login Sekarang!</a></p>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Register Area -->
@endsection