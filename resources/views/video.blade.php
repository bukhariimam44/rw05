@extends('layouts.umum')
@section('content')
<!-- Start Page Banner -->
<div class="page-title-area">
            <div class="container">
                <div class="page-title-content">
                    <h2>Video</h2> 
                    <ul>
                    <li><a href="{{route('index')}}">Beranda</a></li>
                        <li>Video</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Page Banner -->
        
        <!-- Start Default News Area -->
        <section class="default-news-area ptb-50">
            <div class="container">
                <div class="row">
                
                    <div class="col-lg-8">
                        @if(Auth::check())
                        @if(Auth::user()->type == 'Admin')
                        <a href="{{route('tambah-video')}}" class="btn btn-success"><i class='bx bx-plus-circle'></i> Tambah</a> <hr>
                        @endif
                        @endif
                        <div class="row">
                        @foreach($videos as $vd)
                            <div class="col-lg-6 col-md-6">
                                <div class="video-item mb-30">
                                    <div class="video-news-image">
                                        <a href="#">
                                            <img src="{{asset('assets/gambar/video/'.$vd->gambar)}}" alt="image">
                                        </a>
    
                                        <a href="https://www.youtube.com/watch?v={{$vd->key_video}}" class="popup-youtube">
                                            <i class='bx bx-play-circle'></i>
                                        </a>
                                    </div>
    
                                    <div class="video-news-content">
                                        <h3>
                                            <a href="#">{{$vd->judul}}</a>
                                        </h3>
                                        <span>{{date('d M, Y', strtotime($vd->created_at))}}</span> 
                                        @if(Auth::check())
                                        @if(Auth::user()->type == 'Admin')
                                        <a href="{{route('edit-video',$vd->id)}}" class="btn btn-warning float-sm-right"><i class='bx bxs-edit'></i> Edit</a> <a href="{{route('hapus-video',$vd->id)}}" class="btn btn-danger float-sm-right"><i class='bx bxs-trash'></i> Hapus</a>
                                        @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @if(count($videos) < 1)
                            <div class="col-lg-12 col-md-12">
                                <div class="mb-30">
                                <center><label for="" class="text-center">Video Belum ada</label></center>
                                </div>
                            </div>
                        @endif
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <aside class="widget-area">
                            <section class="widget widget_latest_news_thumb">
                                <h3 class="widget-title">Berita Terbaru</h3>

                                @foreach($beritas as $berita)
                                <article class="item">
                                    <a href="#" class="thumb">
                                    <img class="fullimage cover" src="{{asset('assets/gambar/berita/'.$berita->gambar)}}" alt="">
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="{{route('detail-berita',str_replace(' ','_',$berita->judul))}}">{{ substr($berita->judul, 0, 40) }}..</a></h4>
                                        <span>28 September, 2021</span>
                                    </div>
                                </article>
                                @endforeach
                                @if(count($beritas) < 1)
                                <article class="item">
                                    <a href="#" class="thumb">
                                    <img class="fullimage cover" src="{{asset('assets/gambar/berita/download.jpeg')}}" alt="">
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="#">Belum ada berita</a></h4>
                                        <span>## ####, ####</span>
                                    </div>
                                </article>
                                @endif
                            </section>

                            <section class="widget widget_popular_posts_thumb">
                                <h3 class="widget-title">Berita Pengajuan Warga</h3>

                                @foreach($berita_wargas as $berita_warga)
                                <article class="item">
                                    <a href="#" class="thumb">
                                    <img class="fullimage cover" src="{{asset('assets/gambar/berita/'.$berita_warga->gambar)}}" alt="">
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="{{route('detail-berita',str_replace(' ','_',$berita_warga->judul))}}">{{ substr($berita_warga->judul, 0, 40) }}..</a></h4>
                                        <span>{{date('d M Y', strtotime($berita_warga->created_at))}}</span>
                                    </div>
                                </article>
                                @endforeach
                                @if(count($berita_wargas) < 1)
                                <article class="item">
                                    <a href="#" class="thumb">
                                    <img class="fullimage cover" src="{{asset('assets/gambar/berita/download.jpeg')}}" alt="">
                                    </a>
                                    <div class="info">
                                        <h4 class="title usmall"><a href="#">Belum ada berita</a></h4>
                                        <span>## ####, ####</span>
                                    </div>
                                </article>
                                @endif
                            </section>

                            @include('includes.sosmed')

                            @include('includes.logo')

                            @include('includes.berlangganan')
                        </aside>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Default News Area -->
@endsection