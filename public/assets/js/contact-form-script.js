/*==============================================================*/
// Depan Contact Form  JS
/*==============================================================*/
(function ($) {
    "use strict"; // Start of use strict
    $("#contactForm").validator().on("submit", function (event) {
        if (event.isDefaultPrevented()) {
            // handle the invalid form...
            formError();
            submitMSG(false, "Apakah Anda mengisi formulir dengan benar?");
        } else {
            // everything looks good!
            event.preventDefault();
            document.getElementById('contactForm').submit();
            // submitForm();
        }
    });


    function submitForm(){
        // Initiate Variables With Form Content
        var nama = $("#nama").val();
        var email = $("#email").val();
        var judul = $("#judul").val();
        var nohp = $("#nohp").val();
        var pesan = $("#pesan").val();
        var setujui = $("#setujui").val();


        $.ajax({
            type: "POST",
            url: "assets/php/form-process.php",
            data: "nama=" + nama + "&email=" + email + "&judul=" + judul + "&nohp=" + nohp + "&pesan=" + pesan + "&setujui=" + setujui,
            success : function(text){
                if (text == "success"){
                    formSuccess();
                } else {
                    formError();
                    submitMSG(false,text);
                }
            }
        });
    }

    function formSuccess(){
        $("#contactForm")[0].reset();
        submitMSG(true, "Message Submitted!")
    }

    function formError(){
        $("#contactForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $(this).removeClass();
        });
    }

    function submitMSG(valid, msg){
        if(valid){
            var msgClasses = "h4 tada animated text-success";
        } else {
            var msgClasses = "h4 text-danger";
        }
        $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
    }
}(jQuery)); // End of use strict