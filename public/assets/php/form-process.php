<?php

$errorMSG = "";

// NAME
if (empty($_POST["nama"])) {
    $errorMSG = "Name is required ";
} else {
    $nama = $_POST["nama"];
}

// EMAIL
if (empty($_POST["email"])) {
    $errorMSG .= "Email is required ";
} else {
    $email = $_POST["email"];
}

// MSG SUBJECT
if (empty($_POST["judul"])) {
    $errorMSG .= "Subject is required ";
} else {
    $judul = $_POST["judul"];
}

// Phone Number
if (empty($_POST["nohp"])) {
    $errorMSG .= "Number is required ";
} else {
    $nohp = $_POST["nohp"];
}


// MESSAGE
if (empty($_POST["pesan"])) {
    $errorMSG .= "Message is required ";
} else {
    $pesan = $_POST["pesan"];
}


$EmailTo = "bukhariimam44@gmail.com";

$Subject = "New Message Received";

// prepare email body text
$Body = "";
$Body .= "Name: ";
$Body .= $nama;
$Body .= "\n";
$Body .= "Email: ";
$Body .= $email;
$Body .= "\n";
$Body .= "Subject: ";
$Body .= $judul;
$Body .= "\n";
$Body .= "Phone Number: ";
$Body .= $nohp;
$Body .= "\n";
$Body .= "Message: ";
$Body .= $pesan;
$Body .= "\n";

// send email
$success = mail($EmailTo, $Subject, $Body);

// redirect to success page
if ($success && $errorMSG == ""){
   echo "success";
}else{
    if($errorMSG == ""){
        echo "Something went wrong :(";
    } else {
        echo $errorMSG;
    }
}

?>