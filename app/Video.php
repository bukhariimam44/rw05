<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'judul','key_video','gambar','open','admin','created_at','updated_at'
    ];
    public function UserId(){
        return $this->belongsTo('App\User','admin');
    }
}
