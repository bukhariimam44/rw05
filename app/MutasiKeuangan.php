<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MutasiKeuangan extends Model
{
    protected $fillable = [
        'no_trx','tgl_trx','tahun_bulan','masuk','keluar','nominal','mutasi','keterangan','admin_id','open','created_at','updated_at'
    ];
}
