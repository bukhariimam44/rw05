<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use Log;
use DB;

class LoginController extends Controller
{
    public function login(){
        return view('login');
    }
    public function authenticate(Request $request)
    {
        $rules = ['captcha' => 'required|captcha_api:'. request('key') . ',math'];
        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'code'=>400,
                'title'=>'Gagal',
                'icon'=>'error',
                'message'=>'Hasilnya Salah !',
                
            ]);  

        } else {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password],$request->remember)) {
                Log::info('Login berhasil');
                return response()->json([
                    'code'=>200,
                    'title'=>'Berhasil',
                    'icon'=>'success',
                    'message'=>'Berhasil Login',
        
                ]);
            }
            return response()->json([
                'code'=>400,
                'title'=>'Gagal',
                'icon'=>'error',
                'message'=>'Username atau Password Salah',
                
            ]);  
        }
        
    }
    public function daftar(){
        return view('daftar');
    }
    public function postdaftar(Request $request){
        $rules = ['captcha' => 'required|captcha_api:'. request('key') . ',math'];
        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'code'=>400,
                'title'=>'Gagal',
                'icon'=>'error',
                'message'=>'Hasilnya Salah !',
                
            ]);  

        } else {
            $message = [
                'password.min'=>'Password Baru minimal 6 karakter',
            ];
            $validator = Validator::make($request->all(), [
                            'password'  => 'required|min:6',
            ],$message);
            if ($validator->fails()) {
                return response()->json([
                    'code'=>400,
                    'title'=>'Gagal',
                    'icon'=>'error',
                    'message'=>'Password minimal 6 karakter !',
                    
                ]); 
            }
            DB::beginTransaction();
            try {
                User::create([
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'nohp'=>$request->nohp,
                    'rt'=>$request->rt,
                    'password'=>Hash::make($request['password']),
                    'open'=>1,
                    'type'=>'Warga'
                ]);
            } catch (\Throwable $th) {
                Log::info('Gagal Daftar:'.$th->getMessage());
                DB::rollback();
                return response()->json([
                    'code'=>400,
                    'title'=>'Gagal',
                    'icon'=>'error',
                    'message'=>'Daftar gagal'
                ]); 
            }
            DB::commit();
            return response()->json([
                'code'=>200,
                'title'=>'Berhasil',
                'icon'=>'success',
                'message'=>'Berhasil Daftar',
    
            ]);
        }
    }
}
