<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Notifications\InfoUser;
use Illuminate\Support\Facades\Validator;

use App\Berita;
use App\Tentang;
use App\KategoriBerita;
use App\Notifications\BeritaTerbaru;
use App\Video;
use App\Organisasi;
use App\Kontak;
use App\Pengurus;
use App\User;
use App\KomentarBerita;
use App\SyaratKetentuan;
use App\MutasiKeuangan;
use App\Jabatan;
use App\Berlangganan;

use DB;
use Log;
use Image;
use PDF;
use App;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function verification(){
        return view('verify');
    }
    public function home(){
        return view('user.home');
    }
    public function logout(Request $request){
		Auth::logout();
		$request->session()->invalidate();
		$request->session()->regenerateToken();
        flash()->overlay('Berhasil logout', 'INFO');
		return redirect()->route('index');
	}
    public function pengajuan_warga(Request $request){
        $datas = Berita::where('open',0)->where('dari','Warga')->orderBy('id','DESC')->get();
        return view('user.pengajuan_berita_warga',compact('datas'));
    }
    public function setuju_berita_warga($id){
        $datas = Berita::where('open',0)->where('judul',str_replace('_',' ',$id))->where('dari','Warga')->first();
        $datas->open = 1;
        $berlangganan = Berlangganan::get();
        foreach ($berlangganan as $key => $value) {
            sleep(1);
            $value->judul = $datas->judul;
            $value->notify(new BeritaTerbaru($datas));
        }
        $datas->bagikan = 'Sudah';
        if ($datas->update()) {
            flash()->overlay('Berita berhasil disetujui', 'INFO');
            return redirect()->route('berita-pengajuan-warga');
        }
        flash()->overlay('Berita gagal disetujui', 'INFO');
        return redirect()->back();
    }
    public function edit_berita($judul){
        $datas = Berita::where('judul',str_replace('_',' ',$judul))->where('open',1)->where('dari','Admin')->first();
        if (!$datas) {
            return redirect()->back();
        }
        return view('user.edit_berita', compact('datas'));
    }
    public function hapus_berita($id){
        $datas = Berita::find($id);
        $datas->open = 0;
        if ($datas->update()) {
            flash()->overlay('Berita berhasil dihapus', 'INFO');
        }else {
            flash()->overlay('Berita gagal dihapus', 'INFO');
        }
        return redirect()->back();
    }
    public function tambah_berita(Request $request){
        if ($request->action == 'tambah') {
            $validator = Validator::make($request->all(), [
                'judul'=>'required',
                'isi' => 'required',
                'gambar'=>'required'
            ]);
            if ($validator->fails()) {
                flash()->overlay('Isi semua dengan benar', 'INFO');
                return redirect()->back();
            }
            DB::beginTransaction();
            try {
                $edit = new Berita;
                $isi=$request->input('isi');
                $dom = new \DomDocument();
                $dom->loadHtml($isi, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
                $images = $dom->getElementsByTagName('img');
    
                foreach($images as $k => $img){
                    $src = $img->getAttribute('src');
                    if(preg_match('/data:image/', $src)){
                    
                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];
                        
                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/assets/gambar/berita/$filename.$mimetype";
            
                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                          // resize if required
                          /* ->resize(300, 200) */
                          ->encode($mimetype, 100) 	// encode file to the specified mimetype
                          ->save(public_path($filepath));
                        
                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                }
    
                $description = $dom->saveHTML();
                
                $edit->judul = $request->judul;

                $image = $request->file('gambar');
                $imageName = $image->getClientOriginalName();
                $namafile = date('YmdHis')."_".$imageName;
                $directory = public_path('/assets/gambar/berita/');
                // return $directory;
                $imageUrl = $directory.$namafile;
                Image::make($image)->save($imageUrl);
                $edit->gambar = $namafile;
                $edit->kategori_id = $request->kategori;
                $edit->open = 1;
                $edit->isi = $description;
                $edit->admin_id = $request->user()->id;
                $berlangganan = Berlangganan::get();
                foreach ($berlangganan as $key => $value) {
                    sleep(1);
                    $value->judul = $request->judul;
                    $value->notify(new BeritaTerbaru($edit));
                }
                $edit->bagikan = 'Sudah';
                $edit->save();
            } catch (\Throwable $th) {
                Log::info('Gagal Update Berita:'.$th->getMessage());
                DB::rollback();
                flash()->overlay('Berita gagal disimpan', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay('Berita berhasil disimpan', 'INFO');
            return redirect()->route('berita');
            
        }
        $kategories = KategoriBerita::where('open',1)->get();
        return view('user.tambah_berita',compact('kategories'));
    }
    public function update_berita(Request $request, $judul){
        
        $this->validate($request, [
            'judul'=>'required',
            'isi' => 'required',
        ]);
        DB::beginTransaction();
        try {
            $edit = Berita::where('id',$request->ids)->where('open',1)->where('dari','Admin')->first();
            $isi=$request->input('isi');
            $dom = new \DomDocument();
            libxml_use_internal_errors(true);
            @$dom->loadHtml($isi, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD); 
            $dom->preserveWhiteSpace = FALSE;    
            $images = $dom->getElementsByTagName('img');

            foreach($images as $k => $img){
                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){
				
                    // get the mimetype
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];
                    
                    // Generating a random filename
                    $filename = uniqid();
                    $filepath = "/assets/gambar/berita/$filename.$mimetype";
        
                    // @see http://image.intervention.io/api/
                    $image = Image::make($src)
                      // resize if required
                      /* ->resize(300, 200) */
                      ->encode($mimetype, 100) 	// encode file to the specified mimetype
                      ->save(public_path($filepath));
                    
                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } // <!--endif
            }

            $description = $dom->saveHTML();
            
            $edit->judul = $request->judul;
            if ($request->file('gambar')) {
                
                // $this->validate($request, [
                //     'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                // ]);
                Log::info('RE GAMBAR 2');
                $destination_foto =public_path('assets/gambar/berita/'.$edit->gambar);
                if (file_exists($destination_foto)) {
                    unlink($destination_foto);
                }
                $image = $request->file('gambar');
                $imageName = $image->getClientOriginalName();
                $namafile = date('YmdHis')."_".$imageName;
                $directory = public_path('/assets/gambar/berita/');
                // return $directory;
                $imageUrl = $directory.$namafile;
                Image::make($image)->resize(1200, 850)->save($imageUrl);
                $edit->gambar = $namafile;
            }
            $edit->isi = $description;
            $edit->admin_id = $request->user()->id;
            $edit->update();
        } catch (\Throwable $th) {
            Log::info('Gagal Update Berita:'.$th->getMessage());
            DB::rollback();
            flash()->overlay('Berita gagal update', 'INFO');
            return redirect()->back();
        }
        DB::commit();
        flash()->overlay('Berita berhasil update', 'INFO');
        return redirect()->route('berita');
        
    }
    public function edit_tentang(Request $request){
        if ($request->konten) {
            $this->validate($request, [
                'konten' => 'required',
            ]);
            DB::beginTransaction();
            try {
                $edit = Tentang::first();
                $konten=$request->input('konten');
                $dom = new \DomDocument();
                libxml_use_internal_errors(true);
                @$dom->loadHtml($konten, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $dom->preserveWhiteSpace = FALSE;     
                $images = $dom->getElementsByTagName('img');
    
                foreach($images as $k => $img){
                    $src = $img->getAttribute('src');

                    if(preg_match('/data:image/', $src)){
                    
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];
                        
                        $filename = uniqid();
                        $filepath = "/assets/gambar/$filename.$mimetype";
            
                        $image = Image::make($src)
                          // resize if required
                          /* ->resize(300, 200) */
                          ->encode($mimetype, 100) 	// encode file to the specified mimetype
                          ->save(public_path($filepath));
                        
                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                }
                $edit->konten = $dom->saveHTML();
                $edit->admin_update = $request->user()->id;
                $edit->update();
            } catch (\Throwable $th) {
                Log::info('Gagal Add user:'.$th->getMessage());
                DB::rollback();
                flash()->overlay('Data gagal edit', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay('Data berhasil edit', 'INFO');
            return redirect()->route('tentang');
        }
        $edit = Tentang::first();
        return view('user.edit_tentang',compact('edit'));
    }
    public function edit_video(Request $request,$id){
        $edit = Video::where('id',$id)->where('open',1)->first();
        if ($request->judul && $request->video) {
            if ($request->file('gambar')) {
                $destination_foto =public_path('assets/gambar/video/'.$edit->gambar);
                if (file_exists($destination_foto)) {
                    unlink($destination_foto);
                }
                $image = $request->file('gambar');
                $imageName = $image->getClientOriginalName();
                $namafile = date('YmdHis')."_".$imageName;
                $directory = public_path('/assets/gambar/video/');
                // return $directory;
                $imageUrl = $directory.$namafile;
                Image::make($image)->resize(610, 500)->save($imageUrl);
                $edit->gambar = $namafile;
            }
            $edit->judul = $request->judul;
            $edit->key_video = $request->video;
            if ($edit->update()) {
                flash()->overlay('Data berhasil edit', 'INFO');
            }else{
                flash()->overlay('Data gagal edit', 'INFO');
            }
            return redirect()->route('video');

        }
        return view('user.edit_video',compact('edit'));
    }
    public function edit_struktur_organisasi(Request $request){
        if ($request->konten) {
            $this->validate($request, [
                'konten' => 'required',
            ]);
            DB::beginTransaction();
            try {
                $edit = Organisasi::first();
                $konten=$request->input('konten');
                $dom = new \DomDocument();
                libxml_use_internal_errors(true);
                @$dom->loadHtml($konten, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);  
                $dom->preserveWhiteSpace = FALSE;  
                $images = $dom->getElementsByTagName('img');
    
                foreach($images as $k => $img){
                    $src = $img->getAttribute('src');

                    if(preg_match('/data:image/', $src)){
                    
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];
                        
                        $filename = uniqid();
                        $filepath = "/assets/gambar/$filename.$mimetype";
            
                        $image = Image::make($src)
                          // resize if required
                          /* ->resize(300, 200) */
                          ->encode($mimetype, 100) 	// encode file to the specified mimetype
                          ->save(public_path($filepath));
                        
                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                }
                $edit->konten = $dom->saveHTML();
                $edit->admin_update = $request->user()->id;
                $edit->update();
            } catch (\Throwable $th) {
                Log::info('Gagal Add user:'.$th->getMessage());
                DB::rollback();
                flash()->overlay('Data gagal simpan', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay('Data berhasil simpan', 'INFO');
            return redirect()->route('struktur-organisasi');
        }
        $edit = Organisasi::first();
        return view('user.edit_struktur_organisasi',compact('edit'));
    }
    public function tambah_video(Request $request){
        if ($request->judul && $request->file('gambar') && $request->video) {
            DB::beginTransaction();
            try {
                $add = new Video;
                $image = $request->file('gambar');
                $imageName = $image->getClientOriginalName();
                $namafile = date('YmdHis')."_".$imageName;
                $directory = public_path('/assets/gambar/video/');
                // return $directory;
                $imageUrl = $directory.$namafile;
                Image::make($image)->resize(610, 500)->save($imageUrl);
                $add->gambar = $namafile;
                $add->judul = $request->judul;
                $add->key_video = $request->video;
                $add->open = 1;
                $add->admin = $request->user()->id;
                $add->save();
            } catch (\Throwable $th) {
                Log::info('Gagal Add user:'.$th->getMessage());
                DB::rollback();
                flash()->overlay('Video gagal simpan', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay('Video berhasil simpan', 'INFO');
            return redirect()->route('video');
        }
        return view('user.tambah_video');
    }
    public function hapus_video($id){
        $edit = Video::find($id);
        $edit->open = 0;
        if ($edit->update()) {
            flash()->overlay('Video berhasil dihapus', 'INFO');
        }else {
            flash()->overlay('Video gagal dihapus', 'INFO');
        }
        return redirect()->back();
    }
    public function edit_kontak(Request $request){
        $datas = Kontak::find(1);
        if ($request->email && $request->telp && $request->alamat && $request->maps) {
            $datas->maps = $request->maps;
            $datas->email = $request->email;
            $datas->telpon = $request->telp;
            $datas->alamat = $request->alamat;
            $datas->admin = $request->user()->id;
            if ($datas->update()) {
                flash()->overlay('Kontak berhasil diupdate', 'INFO');
            } else {
                flash()->overlay('Kontak gagal diupdate', 'INFO');
            }
            return redirect()->route('kontak');
        }
        return view('user.edit_kontak',compact('datas'));
    }
    public function tambah_pengurus(Request $request){
        if ($request->nama && $request->file('foto') && $request->jabatan) {
            DB::beginTransaction();
            try {
                $add = new Pengurus;
                $image = $request->file('foto');
                $imageName = $image->getClientOriginalName();
                $namafile = date('YmdHis')."_".$imageName;
                $directory = public_path('/assets/gambar/pengurus/');
                // return $directory;
                $imageUrl = $directory.$namafile;
                Image::make($image)->resize(550, 550)->save($imageUrl);
                $add->foto = $namafile;
                $add->nama = $request->nama;
                $add->jabatan_id = $request->jabatan;
                $add->open = 1;
                $add->admin_id = $request->user()->id;
                $add->save();
            } catch (\Throwable $th) {
                Log::info('Gagal Add user:'.$th->getMessage());
                DB::rollback();
                flash()->overlay('Pengurus gagal simpan', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay('Pengurus berhasil simpan', 'INFO');
            return redirect()->route('tentang');

        }
        $jabatans = Jabatan::get();
        return view('user.tambah_pengurus',compact('jabatans'));
    }
    public function edit_pengurus(Request $request,$id){
        $edit = Pengurus::where('id',$id)->where('open',1)->first();
        if ($request->nama && $request->jabatan && $request->action == 'edit') {
            DB::beginTransaction();
            try {
                if ($request->file('foto')) {
                    $destination_foto =public_path('assets/gambar/pengurus/'.$edit->foto);
                    if (file_exists($destination_foto)) {
                        unlink($destination_foto);
                    }
                    $image = $request->file('foto');
                    $imageName = $image->getClientOriginalName();
                    $namafile = date('YmdHis')."_".$imageName;
                    $directory = public_path('/assets/gambar/pengurus/');
                    // return $directory;
                    $imageUrl = $directory.$namafile;
                    Image::make($image)->resize(550, 550)->save($imageUrl);
                    $edit->foto = $namafile;
                }
                
                $edit->nama = $request->nama;
                $edit->jabatan_id = $request->jabatan;
                $edit->admin_id = $request->user()->id;
                $edit->update();
            } catch (\Throwable $th) {
                Log::info('Gagal Add user:'.$th->getMessage());
                DB::rollback();
                flash()->overlay('Pengurus gagal simpan', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay('Pengurus berhasil simpan', 'INFO');
            return redirect()->route('tentang');
        }elseif ($request->action == 'hapus') {
            $edit->open = 0;
            $edit->admin_id = $request->user()->id;
            if ($edit->update()) {
                flash()->overlay('Pengurus berhasil hapus', 'INFO');
                return redirect()->route('tentang');
            }
            flash()->overlay('Pengurus gagal hapus', 'INFO');
            return redirect()->back();
        }
        $jabatans = Jabatan::get();
        return view('user.edit_pengurus',compact('edit','jabatans'));
    }
    public function data_user(Request $request){
        $users = User::where('open',1)->get();
        return view('user.data_user',compact('users'));
    }
    public function tambah_user(Request $request){
        if ($request->action == 'tambah') {
            $pass = str_random(6);
            $add = new User;
            $add->name=$request->name;
            $add->email=$request->email;
            $add->nohp=$request->nohp;
            $add->rt=$request->rt;
            $add->password=Hash::make($pass);
            $add->open=1;
            $add->email_verified_at=date('Y-m-d H:i:s');
            $add->type=$request->sebagai;
            if ($add->save()) {
                $add->katasandi = $pass;
                $add->notify(new InfoUser($add));
                flash()->overlay('User berhasil disimpan dan password telah dikirim ke email.', 'INFO');
                return redirect()->route('data-user');
            }
            flash()->overlay('User gagal disimpan', 'INFO');
            return redirect()->back();
        }
        return view('user.tambah_user');
    }
    public function user($action, $ids){
        if ($action == 'aktifkan') {
            $user = User::find($ids)->update([
                'email_verified_at'=>date('Y-m-d H:i:s')
            ]);
        }elseif ($action == 'nonaktifkan') {
            $user = User::find($ids)->update([
                'email_verified_at'=>NULL
            ]);
        }elseif ($action == 'blokir') {
            $user = User::find($ids)->update([
                'email_verified_at'=>NULL,
                'open'=>0
            ]);
        }
        if ($user) {
            flash()->overlay('User berhasil id '.$action, 'INFO');
            return redirect()->route('data-user');
        }
        flash()->overlay('User gagal di'.$action, 'INFO');
        return redirect()->back();
    }
    public function data_komentar(){
        $datas = KomentarBerita::where('setujui','Tidak')->get();
        return view('user.data_komentar',compact('datas'));
    }
    public function tampilkan_komentar($id){
        $update = KomentarBerita::find($id);
        $update->setujui = 'Ya';
        if ($update->update()) {
            flash()->overlay('Komentar berhasil ditampilkan', 'INFO');
            return redirect()->back();
        }
        flash()->overlay('Komentar gagal ditampilkan', 'INFO');
        return redirect()->back();
    }
    public function tambah_syarat(Request $request){
        if ($request->syarat) {
            $add = SyaratKetentuan::create([
                'syarat'=>$request->syarat
            ]);
            if ($add) {
                flash()->overlay('Data berhasil disimpan', 'INFO');
                return redirect()->route('syarat-dan-ketentuan');
            }
            flash()->overlay('Data gagal disimpan', 'INFO');
            return redirect()->back();
        }
        return view('user.tambah_syarat');
    }
    public function edit_syarat(Request $request, $id){
        $edit = SyaratKetentuan::find($id);
        if ($request->syarat) {
            $edit->syarat = $request->syarat;
            if ($edit->update()) {
                flash()->overlay('Data berhasil diupdate', 'INFO');
                return redirect()->route('syarat-dan-ketentuan');
            }
            flash()->overlay('Data gagal diupdate', 'INFO');
            return redirect()->back();
        }
        return view('user.edit_syarat',compact('edit'));
    }
    public function hapus_syarat($id){
        $edit = SyaratKetentuan::find($id);
        if ($edit->delete()){
            flash()->overlay('Data berhasil dihapus', 'INFO');
            return redirect()->route('syarat-dan-ketentuan');
        }
        flash()->overlay('Data gagal dihapus', 'INFO');
        return redirect()->back();
    }
    public function data_uang_masuk(Request $request){
        $datas = MutasiKeuangan::where('open',1)->where('mutasi','Kredit')->orderBy('tgl_trx','ASC')->get();
        return view('user.data_uang_masuk',compact('datas'));
    }
    public function tambah_uang_masuk(Request $request){
        if ($request->tanggal && $request->nominal && $request->keterangan) {
            $add = MutasiKeuangan::create([
                'no_trx'=>date('Ymdhis').$request->user()->id,
                'tgl_trx'=>$request->tanggal,
                'tahun_bulan'=>date('Y-m', strtotime($request->tanggal)),
                'masuk'=>str_replace(".","",$request->nominal),
                'keluar'=>0,
                'nominal'=>str_replace(".","",$request->nominal),
                'mutasi'=>'Kredit',
                'keterangan'=>$request->keterangan,
                'admin_id'=>$request->user()->id,
                'open'=>1
            ]);
            if ($add) {
                flash()->overlay('Data berhasil ditambah', 'INFO');
                return redirect()->route('data-uang-masuk');
            }
            flash()->overlay('Data gagal ditambah', 'INFO');
            return redirect()->back();
        }
        return view('user.tambah_uang_masuk');
    }
    public function edit_uang_masuk(Request $request, $id){
        $edit = MutasiKeuangan::where('id',$id)->where('mutasi','Kredit')->first();
        if ($request->tanggal && $request->nominal && $request->keterangan) {
            $edit->tgl_trx = $request->tanggal;
            $edit->tahun_bulan = date('Y-m', strtotime($request->tanggal));
            $edit->masuk = str_replace(".","",$request->nominal);
            $edit->keluar = 0;
            $edit->nominal = str_replace(".","",$request->nominal);
            $edit->keterangan = $request->keterangan;
            if ($edit->update()) {
                flash()->overlay('Data berhasil diedit', 'INFO');
                return redirect()->route('data-uang-masuk');
            }
            flash()->overlay('Data gagal diedit', 'INFO');
            return redirect()->back();
        }
        return view('user.edit_uang_masuk',compact('edit'));
    }
    public function hapus_uang_masuk($id){

    }
    public function data_uang_keluar(Request $request){
        $datas = MutasiKeuangan::where('open',1)->orderBy('tgl_trx','ASC')->where('mutasi','Debet')->get();
        return view('user.data_uang_keluar',compact('datas'));
    }
    public function tambah_uang_keluar(Request $request){
        if ($request->tanggal && $request->nominal && $request->keterangan) {
            $add = MutasiKeuangan::create([
                'no_trx'=>date('Ymdhis').$request->user()->id,
                'tgl_trx'=>$request->tanggal,
                'tahun_bulan'=>date('Y-m', strtotime($request->tanggal)),
                'masuk'=>0,
                'keluar'=>str_replace(".","",$request->nominal),
                'nominal'=>str_replace(".","",$request->nominal),
                'mutasi'=>'Debet',
                'keterangan'=>$request->keterangan,
                'admin_id'=>$request->user()->id,
                'open'=>1
            ]);
            if ($add) {
                flash()->overlay('Data berhasil ditambah', 'INFO');
                return redirect()->route('data-uang-keluar');
            }
            flash()->overlay('Data gagal ditambah', 'INFO');
            return redirect()->back();
        }
        return view('user.tambah_uang_keluar');
    }
    public function edit_uang_keluar(Request $request, $id){
        $edit = MutasiKeuangan::where('id',$id)->where('mutasi','Debet')->first();
        if ($request->tanggal && $request->nominal && $request->keterangan) {
            $edit->tgl_trx = $request->tanggal;
            $edit->tahun_bulan = date('Y-m', strtotime($request->tanggal));
            $edit->masuk = 0;
            $edit->keluar = str_replace(".","",$request->nominal);
            $edit->nominal = str_replace(".","",$request->nominal);
            $edit->keterangan = $request->keterangan;
            if ($edit->update()) {
                flash()->overlay('Data berhasil diedit', 'INFO');
                return redirect()->route('data-uang-keluar');
            }
            flash()->overlay('Data gagal diedit', 'INFO');
            return redirect()->back();
        }
        return view('user.edit_uang_keluar',compact('edit'));
    }
    public function mutasi_keuangan(){
        $datas = MutasiKeuangan::where('open',1)->orderBy('tgl_trx','ASC')->get();
        return view('user.mutasi_keuangan',compact('datas'));
    }
    //WARGA
    public function add_berita(Request $request){
        if ($request->action == 'tambah') {
            $validator = Validator::make($request->all(), [
                'judul'=>'required',
                'isi' => 'required',
                'gambar'=>'required'
            ]);
            if ($validator->fails()) {
                flash()->overlay('Isi semua dengan benar', 'INFO');
                return redirect()->back();
            }
            DB::beginTransaction();
            try {
                $edit = new Berita;
                $isi=$request->input('isi');
                $dom = new \DomDocument();
                $dom->loadHtml($isi, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
                $images = $dom->getElementsByTagName('img');
    
                foreach($images as $k => $img){
                    $src = $img->getAttribute('src');
                    if(preg_match('/data:image/', $src)){
                    
                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];
                        
                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/assets/gambar/berita/$filename.$mimetype";
            
                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                          // resize if required
                          /* ->resize(300, 200) */
                          ->encode($mimetype, 100) 	// encode file to the specified mimetype
                          ->save(public_path($filepath));
                        
                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                }
    
                $description = $dom->saveHTML();
                
                $edit->judul = $request->judul;

                $image = $request->file('gambar');
                $imageName = $image->getClientOriginalName();
                $namafile = date('YmdHis')."_".$imageName;
                $directory = public_path('/assets/gambar/berita/');
                // return $directory;
                $imageUrl = $directory.$namafile;
                Image::make($image)->save($imageUrl);
                $edit->gambar = $namafile;
                $edit->kategori_id = $request->kategori;
                $edit->open = 0;
                $edit->isi = $description;
                $edit->admin_id = $request->user()->id;
                $edit->dari = 'Warga';
                $edit->save();
            } catch (\Throwable $th) {
                Log::info('Gagal Update Berita:'.$th->getMessage());
                DB::rollback();
                flash()->overlay('Berita gagal disimpan', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay('Berita berhasil disimpan dan akan dicek kembali oleh admin sebelum ditampilkan.', 'INFO');
            return redirect()->route('berita');
            
        }
        $kategories = KategoriBerita::where('open',1)->get();
        return view('user.add_berita_warga',compact('kategories'));
    }
    public function laporan_keuangan(Request $request){
        $result = DB::table('mutasi_keuangans')
                ->select('tahun_bulan',DB::raw('SUM(mutasi_keuangans.masuk) as total_masuk'),DB::raw('SUM(mutasi_keuangans.keluar) as total_keluar'),DB::raw('SUM(mutasi_keuangans.masuk - mutasi_keuangans.keluar) as akumulasi'))
                ->where('open',1)
                ->groupBy('tahun_bulan')
                ->orderBy('tahun_bulan','ASC')
                ->get();
        $datas = json_decode($result, true);
        return view('laporan.keuangan',compact('datas'));
    }
    public function download_laporan(Request $request){
        $masuk = MutasiKeuangan::where('tahun_bulan',$request->tahun_bulan)->where('mutasi','Kredit')->where('open',1)->get();
        $keluar = MutasiKeuangan::where('tahun_bulan',$request->tahun_bulan)->where('mutasi','Debet')->where('open',1)->get();
        $data = [
            'masuk'=>$masuk,
            'keluar'=>$keluar,
            'tahun_bulan'=>$request->tahun_bulan,
            'saldo_awal'=>$request->saldo_awal,
            'saldo_akhir'=> $request->saldo_akhir,
            'ketua'=>Pengurus::where('jabatan_id',1)->where('open',1)->first(),
            'sekretaris'=>Pengurus::where('jabatan_id',4)->where('open',1)->first(),
        ];
        $pdf = PDF::loadView('laporan.pdf', compact('data'));
        // return $pdf->download('laporan-'.$request->tahun_bulan.'.pdf');
        return $pdf->stream();
    }
    public function ganti_password(Request $request){
        if ($request->ps_lm && $request->ps_br) {
            $message = [
                'ps_lm.min'=>'Password Lama minimal 6 karakter',
                'ps_br.min'=>'Password Baru minimal 6 karakter',
            ];
            $response = array('response' => '', 'success'=>false);
            $validator = Validator::make($request->all(), [
                            'ps_lm'  => 'required|min:6',
                            'ps_br'  => 'required|min:6',
            ],$message);
            if ($validator->fails()) {
                $response['response'] = $validator->messages();
                Log::info('Gagal Daftar :'.$validator->messages());
                flash()->overlay('Password Minimal 6 karakter', 'INFO');
                return redirect()->back();
            }
            $user = User::where('email', '=', $request->user()->email)->first();
            if (Hash::check($request->ps_lm, $user->password)) {
                $user->password = Hash::make($request['ps_br']);
                if ($user->update()) {
                    Auth::logout();
                    $request->session()->invalidate();
                    $request->session()->regenerateToken();
                    flash()->overlay('Password Berhasil Diganti', 'INFO');
                    return redirect()->route('index');
                }
                flash()->overlay('Password Gagal Diganti', 'INFO');
                return redirect()->back();
            }else {
                flash()->overlay('Password Lama Salah', 'INFO');
                return redirect()->back();
            }
        }
        return view('user.ganti_password');
    }
    public function data_jabatan(Request $request){
        $datas = Jabatan::get();
        return view('user.data_jabatan',compact('datas'));
    }
    public function tambah_jabatan(Request $request){
        if ($request->jabatan) {
            $add = Jabatan::create([
                'jabatan'=>$request->jabatan
            ]);
            if ($add) {
                flash()->overlay('Data Berhasil ditambah', 'INFO');
                return redirect()->route('data-jabatan');
            }
            flash()->overlay('Data Gagal ditambah', 'INFO');
            return redirect()->back();
        }
        return view('user.tambah_jabatan');
    }
    public function edit_jabatan(Request $request,$id){
        $edit = Jabatan::find($id);
        if ($request->jabatan) {
            $edit->jabatan = $request->jabatan;
            if ($edit->update()) {
                flash()->overlay('Data Berhasil diupdate', 'INFO');
                return redirect()->route('data-jabatan');
            }
            flash()->overlay('Data Gagal diupdate', 'INFO');
            return redirect()->back();
        }
        return view('user.edit_jabatan',compact('edit'));
    }
}
