<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Berita;
use App\Kontak;
use App\Tentang;
use App\Organisasi;
use App\Pengurus;
use App\Video;
use App\KomentarBerita;
use App\SyaratKetentuan;
use App\Berlangganan;
use App\Notifications\KirimPesan;
use App\Notifications\PesanMasuk;
use App\Notifications\BerlanggananBerita;


class UmumController extends Controller
{
    public function index(){
        $utamas = Berita::where('utama','yes')->first();
        $berita_wargas = Berita::where('open',1)->where('dari','Warga')->orderBy('id','DESC')->paginate(5);
        $umum = Berita::where('open',1)->where('dari','Admin')->where('kategori_id',1)->orderBy('id','DESC')->first();
        $politik = Berita::where('open',1)->where('dari','Admin')->where('kategori_id',2)->orderBy('id','DESC')->first();
        $olahraga = Berita::where('open',1)->where('dari','Admin')->where('kategori_id',3)->orderBy('id','DESC')->first();
        $teknologi = Berita::where('open',1)->where('dari','Admin')->where('kategori_id',4)->orderBy('id','DESC')->first();
        $bisnis = Berita::where('open',1)->where('dari','Admin')->where('kategori_id',5)->orderBy('id','DESC')->first();
        $budaya = Berita::where('open',1)->where('dari','Admin')->where('kategori_id',6)->orderBy('id','DESC')->first();
        $hiburan = Berita::where('open',1)->where('dari','Admin')->where('kategori_id',7)->orderBy('id','DESC')->paginate(6);
        $all = [
            'umum'=>$umum,
            'politik'=>$politik,
            'olahraga'=>$olahraga,
            'teknologi'=>$teknologi,
            'bisnis'=>$bisnis,
            'budaya'=>$budaya,
            'hiburan'=> $hiburan
        ];
        return view('index',compact('utamas','berita_wargas','all'));
    }
    public function tentang(){
        $datas = Tentang::first();
        $penguruses = Pengurus::where('open','=',1)->get();
        return view('tentang',compact('datas','penguruses'));
    }
    public function berita(){
        $datas = Berita::where('open',1)->where('dari','Admin')->orderBy('id','DESC')->paginate(10);
        $berita_wargas = Berita::where('open',1)->where('dari','Warga')->orderBy('id','DESC')->paginate(5);
        return $datas->links('berita',compact('datas','berita_wargas'));
    }

    public function detail_berita($judul){
        $datas = Berita::where('judul',str_replace('_',' ',$judul))->first();
        if (!$datas) {
            return redirect()->back();
        }
        
        if (!$sesudah = Berita::where('id', '>', $datas->id)->where('dari','Admin')->where('open',1)->orderBy('id','asc')->first()) {
            $sesudah = "kosong";
        }
        if (!$sebelum = Berita::where('id', '<', $datas->id)->where('dari','Admin')->where('open',1)->orderBy('id','desc')->first()) {
            $sebelum = "kosong";
        }
        $beritas = Berita::where('open',1)->where('dari','Admin')->orderBy('id','DESC')->paginate(5);
        $berita_wargas = Berita::where('open',1)->where('dari','Warga')->orderBy('id','DESC')->paginate(5);
        return view('detail_berita', compact('datas','sebelum','sesudah','beritas','berita_wargas'));
    }
    public function komentar($id){
        $data = KomentarBerita::where('berita_id',$id)->where('setujui','Ya')->get();
        if (Auth::check()) {
            $user = [
                'name'=>Auth::user()->name,
                'email'=>Auth::user()->email,
                'nohp'=>Auth::user()->nohp
            ];
        }else {
            $user = [
                'name'=>'',
                'email'=>'',
                'nohp'=>''
            ];
        }
        return response()->json([
            'total'=>count($data),
            'data'=>$data,
            'user'=>$user
        ]);
    }
    public function postkomentar(Request $request){
        $add =KomentarBerita::create([
            'berita_id'=>$request->ids,
            'nama'=>$request->nama,
            'email'=>$request->email,
            'nohp'=>$request->nohp,
            'komentar'=>$request->komentar,
            'setujui'=>'Tidak'
        ]);
        if ($add) {
            return response()->json([
                'code'=>200,
                'title'=>'Berhasil',
                'icon'=>'success',
                'message'=>'Komentar berhasil terkirim dan menunggu persetujuan admin untuk di tampilkan.'
            ]);
        }
        return response()->json([
            'code'=>400,
            'title'=>'Gagal',
            'icon'=>'success',
            'message'=>'Komentar gagal terkirim'
        ]);
    }
    public function kontak(Request $request){
        $datas = Kontak::first();
        if ($request->action == 'pesan') {
            $datas->nama = $request->nama;
            $datas->judul = $request->judul;
            $datas->email_pengirim = $request->email;
            $datas->nohp = $request->nohp;
            $datas->pesan = $request->pesan;
            $datas->notify(new KirimPesan($datas));
            flash()->overlay('Pesan berhasil dikirim', 'INFO');
        }
        return view('kontak',compact('datas'));
    }
    public function video(){
        $videos = Video::where('open',1)->orderBy('id','DESC')->get();
        $beritas = Berita::where('open',1)->where('dari','Admin')->orderBy('id','DESC')->paginate(5);
        $berita_wargas = Berita::where('open',1)->where('dari','Warga')->orderBy('id','DESC')->paginate(5);
        return view('video',compact('beritas','berita_wargas','videos'));
    }
    public function pengurus(){
        $datas = Pengurus::where('open','=',1)->get();
        return view('pengurus',compact('datas'));
    }
    public function struktur_organisasi(){
        $datas = Organisasi::first();
        return view('struktur_organisasi',compact('datas'));
    }
    public function syarat(){
        $datas = SyaratKetentuan::get();
        return view('syarat_ketentuan',compact('datas'));
    }
    public function berlangganan(Request $request){
        $add = Berlangganan::updateOrcreate([
            'email'=>$request->email
        ]);
        $add->notify(new BerlanggananBerita($add));
        if ($add) {
            return response()->json([
                'code'=>'200',
                'icon'=>'success',
                'title'=>'Berhasil',
                'message'=>'Berhasil berlangganan'
            ]);
        }
        return response()->json([
            'code'=>'400',
            'icon'=>'error',
            'title'=>'Gagal',
            'message'=>'Gagal berlangganan'
        ]);
    }
    public function cari_berita(Request $request){
        $datas = Berita::where('isi','LIKE','%'.$request->cari.'%')->where('open',1)->where('dari','Admin')->orderBy('id','DESC')->paginate(10);
        $berita_wargas = Berita::where('open',1)->where('dari','Warga')->orderBy('id','DESC')->paginate(5);
        return $datas->links('berita',compact('datas','berita_wargas'));
    }
}
