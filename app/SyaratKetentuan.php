<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SyaratKetentuan extends Model
{
    protected $fillable = [
        'syarat','created_at','updated_at'
    ];
}
