<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tentang extends Model
{
    protected $fillable = [
        'konten','admin_update','created_at','updated_at'
    ];
    public function UserId(){
        return $this->belongsTo('App\User','admin_update');
    }
}
