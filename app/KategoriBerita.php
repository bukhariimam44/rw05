<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriBerita extends Model
{
    protected $fillable = [
        'kategori','open','created_at','updated_at'
    ];
}
