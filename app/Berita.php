<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $fillable = [
        'kategori_id','utama','gambar','judul', 'isi','bagikan','admin_id','open','created_at','updated_at'
    ];
    public function KategoriId(){
        return $this->belongsTo('App\KategoriBerita','kategori_id');
    }
    public function UserId(){
        return $this->belongsTo('App\User','admin_id');
    }
}
