<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengurus extends Model
{
    protected $fillable = [
        'nama','foto','jabatan_id','admin_id','open','created_at','updated_at'
    ];
    public function jabatanId(){
        return $this->belongsTo('App\Jabatan','jabatan_id');
    }
}
