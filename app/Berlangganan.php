<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Berlangganan extends Model
{
    use Notifiable;
    
    protected $fillable = [
        'email','created_at','updated_at'
    ];
}
