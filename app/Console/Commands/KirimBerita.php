<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Berita;
use App\Notifications\BeritaTerbaru;
use App\Berlangganan;
use Log;

class KirimBerita extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:beritaTerbaru';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Berita Terbaru';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $berita = Berita::where('bagikan','Belum')->where('open',1)->first();
        $berlangganan = Berlangganan::get();
        foreach ($berlangganan as $key => $value) {
            sleep(1);
            $value->judul = $berita->judul;
            $value->notify(new BeritaTerbaru($value));
        }
        $berita->bagikan = 'Sudah';
        $berita->update();
        Log::info('Berhasil update');
        return $berita;
        
        

        
    }
}
