<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organisasi extends Model
{
    protected $fillable = [
        'konten','admin_update','created_at','updated_at'
    ];
}
