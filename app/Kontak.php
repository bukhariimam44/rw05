<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Kontak extends Model
{
    use Notifiable;
    
    protected $fillable = [
        'maps','email','telpon','alamat','admin','created_at','updated_at'
    ];
}
