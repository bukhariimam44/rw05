<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomentarBerita extends Model
{
    protected $fillable = [
        'berita_id','nama','email','nohp','komentar','setujui','created_at','updated_at'
    ];
    public function beritaId(){
        return $this->belongsTo('App\Berita','berita_id');
    }
}
