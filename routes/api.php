<?php

use Illuminate\Http\Request;
use App\Http\Controllers\UmumController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/berlangganan', [UmumController::class, 'berlangganan'])->name('berlangganan');
Route::post('/berlangganan', [UmumController::class, 'berlangganan'])->name('berlangganan');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
