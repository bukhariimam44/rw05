<?php

use App\Http\Controllers\UmumController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UmumController::class, 'index'])->name('index');
Route::get('/kontak', [UmumController::class, 'kontak'])->name('kontak');
Route::post('/kontak', [UmumController::class, 'kontak'])->name('kontak');
Route::get('/berita', [UmumController::class, 'berita'])->name('berita');
Route::get('/berita/{judul}', [UmumController::class, 'detail_berita'])->name('detail-berita');
Route::get('/komentar/{id}', [UmumController::class, 'komentar'])->name('komentar');
Route::post('/komentar', [UmumController::class, 'postkomentar'])->name('postkomentar');

Route::get('/tentang', [UmumController::class, 'tentang'])->name('tentang');
Route::get('/video', [UmumController::class, 'video'])->name('video');
Route::get('/galeri', [UmumController::class, 'galeri'])->name('galeri');
Route::get('/pengurus', [UmumController::class, 'pengurus'])->name('pengurus');
Route::get('/struktur-organisasi', [UmumController::class, 'struktur_organisasi'])->name('struktur-organisasi');

Route::get('/login', [LoginController::class, 'login'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate'])->name('login');
Route::get('/daftar', [LoginController::class, 'daftar'])->name('daftar');
Route::post('/daftar', [LoginController::class, 'postdaftar'])->name('postdaftar');

Route::get('/syarat-dan-ketentuan', [UmumController::class, 'syarat'])->name('syarat-dan-ketentuan');
Route::get('/tambah-syarat-dan-ketentuan', [HomeController::class, 'tambah_syarat'])->name('tambah-syarat-dan-ketentuan')->middleware(['admin','verified']);
Route::post('/tambah-syarat-dan-ketentuan', [HomeController::class, 'tambah_syarat'])->name('tambah-syarat-dan-ketentuan')->middleware(['admin','verified']);
Route::get('/edit-syarat-dan-ketentuan/{id}', [HomeController::class, 'edit_syarat'])->name('edit-syarat-dan-ketentuan')->middleware(['admin','verified']);
Route::post('/edit-syarat-dan-ketentuan/{id}', [HomeController::class, 'edit_syarat'])->name('edit-syarat-dan-ketentuan')->middleware(['admin','verified']);
Route::get('/hapus-syarat-dan-ketentuan/{id}', [HomeController::class, 'hapus_syarat'])->name('hapus-syarat-dan-ketentuan')->middleware(['admin','verified']);
Route::get('/cari-berita', [UmumController::class, 'cari_berita'])->name('cari-berita');

Route::get('/home', [HomeController::class, 'home'])->name('home')->middleware('auth');

//ADMIN
Route::get('/verifikasi', [HomeController::class, 'verification'])->name('verification.notice')->middleware('auth');
Route::get('/tambah-berita', [HomeController::class, 'tambah_berita'])->name('tambah-berita')->middleware(['admin','verified']);
Route::post('/tambah-berita', [HomeController::class, 'tambah_berita'])->name('tambah-berita')->middleware(['admin','verified']);
Route::get('/berita-pengajuan-warga', [HomeController::class, 'pengajuan_warga'])->name('berita-pengajuan-warga')->middleware(['admin','verified']);
Route::get('/setuju-berita-warga/{id}', [HomeController::class, 'setuju_berita_warga'])->name('setuju-berita-warga')->middleware(['admin','verified']);
Route::get('/edit-berita/{judul}', [HomeController::class, 'edit_berita'])->name('edit-berita')->middleware(['admin','verified']);
Route::get('/hapus-berita/{id}', [HomeController::class, 'hapus_berita'])->name('hapus-berita')->middleware(['admin','verified']);
Route::get('/edit-tentang', [HomeController::class, 'edit_tentang'])->name('edit-tentang')->middleware(['admin','verified']);
Route::post('/edit-tentang', [HomeController::class, 'edit_tentang'])->name('edit-tentang')->middleware(['admin','verified']);
Route::post('/update-berita/{judul}', [HomeController::class, 'update_berita'])->name('update-berita')->middleware(['admin','verified']);
Route::get('/edit-video/{id}', [HomeController::class, 'edit_video'])->name('edit-video')->middleware(['admin','verified']);
Route::get('/hapus-video/{id}', [HomeController::class, 'hapus_video'])->name('hapus-video')->middleware(['admin','verified']);
Route::post('/edit-video/{id}', [HomeController::class, 'edit_video'])->name('edit-video')->middleware(['admin','verified']);
Route::get('/tambah-video', [HomeController::class, 'tambah_video'])->name('tambah-video')->middleware(['admin','verified']);
Route::post('/tambah-video', [HomeController::class, 'tambah_video'])->name('tambah-video')->middleware(['admin','verified']);
Route::get('/tambah-pengurus', [HomeController::class, 'tambah_pengurus'])->name('tambah-pengurus')->middleware(['admin','verified']);
Route::post('/tambah-pengurus', [HomeController::class, 'tambah_pengurus'])->name('tambah-pengurus')->middleware(['admin','verified']);
Route::get('/edit-pengurus/{id}', [HomeController::class, 'edit_pengurus'])->name('edit-pengurus')->middleware(['admin','verified']);
Route::post('/edit-pengurus/{id}', [HomeController::class, 'edit_pengurus'])->name('edit-pengurus')->middleware(['admin','verified']);

Route::get('/edit-struktur-organisasi', [HomeController::class, 'edit_struktur_organisasi'])->name('edit-struktur-organisasi')->middleware(['admin','verified']);
Route::post('/edit-struktur-organisasi', [HomeController::class, 'edit_struktur_organisasi'])->name('edit-struktur-organisasi')->middleware(['admin','verified']);

Route::get('/edit-kontak', [HomeController::class, 'edit_kontak'])->name('edit-kontak')->middleware(['admin','verified']);
Route::post('/edit-kontak', [HomeController::class, 'edit_kontak'])->name('edit-kontak')->middleware(['admin','verified']);

//
Route::get('/data-user', [HomeController::class, 'data_user'])->name('data-user')->middleware(['admin','verified']);
Route::get('/tambah-user', [HomeController::class, 'tambah_user'])->name('tambah-user')->middleware(['admin','verified']);
Route::post('/tambah-user', [HomeController::class, 'tambah_user'])->name('tambah-user')->middleware(['admin','verified']);
Route::get('/user/{action}/{ids}', [HomeController::class, 'user'])->name('user')->middleware(['admin','verified']);
Route::get('/data-komentar', [HomeController::class, 'data_komentar'])->name('data-komentar')->middleware(['admin','verified']);
Route::get('/tampilkan-komentar/{id}', [HomeController::class, 'tampilkan_komentar'])->name('tampilkan-komentar')->middleware(['admin','verified']);
//Keuangan
Route::get('/data-uang-masuk', [HomeController::class, 'data_uang_masuk'])->name('data-uang-masuk')->middleware(['verified']);
Route::get('/tambah-uang-masuk', [HomeController::class, 'tambah_uang_masuk'])->name('tambah-uang-masuk')->middleware(['admin','verified']);
Route::post('/tambah-uang-masuk', [HomeController::class, 'tambah_uang_masuk'])->name('tambah-uang-masuk')->middleware(['admin','verified']);
Route::get('/edit-uang-masuk/{id}', [HomeController::class, 'edit_uang_masuk'])->name('edit-uang-masuk')->middleware(['admin','verified']);
Route::post('/edit-uang-masuk/{id}', [HomeController::class, 'edit_uang_masuk'])->name('edit-uang-masuk')->middleware(['admin','verified']);
Route::get('/hapus-uang-masuk/{id}', [HomeController::class, 'hapus_uang_masuk'])->name('hapus-uang-masuk')->middleware(['admin','verified']);

Route::get('/data-uang-keluar', [HomeController::class, 'data_uang_keluar'])->name('data-uang-keluar')->middleware(['verified']);
Route::get('/tambah-uang-keluar', [HomeController::class, 'tambah_uang_keluar'])->name('tambah-uang-keluar')->middleware(['admin','verified']);
Route::post('/tambah-uang-keluar', [HomeController::class, 'tambah_uang_keluar'])->name('tambah-uang-keluar')->middleware(['admin','verified']);
Route::get('/edit-uang-keluar/{id}', [HomeController::class, 'edit_uang_keluar'])->name('edit-uang-keluar')->middleware(['admin','verified']);
Route::post('/edit-uang-keluar/{id}', [HomeController::class, 'edit_uang_keluar'])->name('edit-uang-keluar')->middleware(['admin','verified']);
Route::get('/hapus-uang-keluar/{id}', [HomeController::class, 'hapus_uang_keluar'])->name('hapus-uang-keluar')->middleware(['admin','verified']);
Route::get('/data-jabatan', [HomeController::class, 'data_jabatan'])->name('data-jabatan')->middleware(['admin','verified']);
Route::get('/tambah-jabatan', [HomeController::class, 'tambah_jabatan'])->name('tambah-jabatan')->middleware(['admin','verified']);
Route::post('/tambah-jabatan', [HomeController::class, 'tambah_jabatan'])->name('tambah-jabatan')->middleware(['admin','verified']);
Route::get('/edit-jabatan/{id}', [HomeController::class, 'edit_jabatan'])->name('edit-jabatan')->middleware(['admin','verified']);
Route::post('/edit-jabatan/{id}', [HomeController::class, 'edit_jabatan'])->name('edit-jabatan')->middleware(['admin','verified']);

Route::get('/mutasi-keuangan', [HomeController::class, 'mutasi_keuangan'])->name('mutasi-keuangan')->middleware(['verified']);
Route::get('/laporan-keuangan', [HomeController::class, 'laporan_keuangan'])->name('laporan-keuangan')->middleware(['verified']);
Route::post('/laporan-keuangan', [HomeController::class, 'download_laporan'])->name('laporan-keuangan')->middleware(['verified']);
//Warga
Route::get('/add-berita', [HomeController::class, 'add_berita'])->name('add-berita')->middleware(['verified']);
Route::post('/add-berita', [HomeController::class, 'add_berita'])->name('add-berita')->middleware(['verified']);

Route::get('/ganti-password', [HomeController::class, 'ganti_password'])->name('ganti-password')->middleware(['verified']);
Route::post('/ganti-password', [HomeController::class, 'ganti_password'])->name('ganti-password')->middleware(['verified']);

// Route::post('/download-laporan', [HomeController::class, 'download_laporan'])->name('download-laporan')->middleware(['verified']);

Route::get('/logout', [HomeController::class, 'logout'])->name('logout');
